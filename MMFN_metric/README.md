# MMFN
#### step1
download model parameters files(~50M) from ' https://imcl.ustc.edu.cn/owncloud/index.php/s/L8H45Jsc334XQkX' and put them under folder 'cps/'. 

passward: metricimcl

besides, pretrained vgg model file(~500M) will be automatically downloaded if you have not use it before.

#### step2
use MMFN to compute score of a distorted image and a reference image
you can use our model in command line like:
```bash
python MMFN.py --r_image /path/of/reference_image --d_image /path/of/distorted_image --gpu_id 0
```
or use our model in python file like:
```python
import utils
from MMFN import MMFN

metric=MMFN() #initialize metric
ref_img=utils.loadimg('ref.png') #load reference image, size:3xHxW, range: 0~1 numpy array
dis_img=utils.loadimg('dis.png') #load distorated image, size:3xHxW, range: 0~1 numpy array
score=metric.predice_score(dis_img, ref_img) #calculate score, score range: 0~1, smaller score means better quality
```

contact me at: pyd@mail.ustc.edu.cn if you have any problem using this metric.