// Copyright (c) 2021-2022, InterDigital Communications, Inc
// All rights reserved.

// Redistribution and use in source and binary forms, with or without 
// modification, are permitted (subject to the limitations in the disclaimer 
// below) provided that the following conditions are met:

// * Redistributions of source code must retain the above copyright notice, 
// this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice, 
// this list of conditions and the following disclaimer in the documentation 
// and/or other materials provided with the distribution.
// * Neither the name of InterDigital Communications, Inc nor the names of its 
// contributors may be used to endorse or promote products derived from this 
// software without specific prior written permission.

// NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY 
// THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
// CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT 
// NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
// OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


/* Rans64 extensions from:
 * https://fgiesen.wordpress.com/2015/12/21/rans-in-practice/
 * Unbounded range coding from:
 * https://github.com/tensorflow/compression/blob/master/tensorflow_compression/cc/kernels/unbounded_index_range_coding_kernels.cc
 **/

#include "rans_interface.hpp"

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <algorithm>
#include <array>
#include <cassert>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>

#include "rans64.h"

namespace py = pybind11;

/* probability range, this could be a parameter... */
constexpr int precision = 16;

constexpr uint16_t bypass_precision = 4; /* number of bits in bypass mode */
constexpr uint16_t max_bypass_val = (1 << bypass_precision) - 1;

namespace {

/* We only run this in debug mode as its costly... */
void assert_cdfs(const std::vector<std::vector<int>> &cdfs,
                 const std::vector<int> &cdfs_sizes) {
  for (int i = 0; i < static_cast<int>(cdfs.size()); ++i) {
    assert(cdfs[i][0] == 0);
    assert(cdfs[i][cdfs_sizes[i] - 1] == (1 << precision));
    for (int j = 0; j < cdfs_sizes[i] - 1; ++j) {
      assert(cdfs[i][j + 1] > cdfs[i][j]);
    }
  }
}

/* Support only 16 bits word max */
inline void Rans64EncPutBits(Rans64State *r, uint32_t **pptr, uint32_t val,
                             uint32_t nbits) {
  assert(nbits <= 16);
  assert(val < (1u << nbits));

  /* Re-normalize */
  uint64_t x = *r;
  uint32_t freq = 1 << (16 - nbits);
  uint64_t x_max = ((RANS64_L >> 16) << 32) * freq;
  if (x >= x_max) {
    *pptr -= 1;
    **pptr = (uint32_t)x;
    x >>= 32;
    Rans64Assert(x < x_max);
  }

  /* x = C(s, x) */
  *r = (x << nbits) | val;
}

inline uint32_t Rans64DecGetBits(Rans64State *r, uint32_t **pptr,
                                 uint32_t n_bits) {
  uint64_t x = *r;
  uint32_t val = x & ((1u << n_bits) - 1);

  /* Re-normalize */
  x = x >> n_bits;
  if (x < RANS64_L) {
    x = (x << 32) | **pptr;
    *pptr += 1;
    Rans64Assert(x >= RANS64_L);
  }

  *r = x;

  return val;
}
} // namespace

void RansEncoder::Encode(const uint32_t lower, const uint32_t upper) 
{
    _syms.push_back({static_cast<uint16_t>(lower),
                     static_cast<uint16_t>(upper-lower),
                     false});
}

void RansEncoder::Encode_Batch(const std::vector<uint32_t> &lowers, const std::vector<uint32_t> &uppers)
{
	  for (int i = 0; i < static_cast<int>(lowers.size()); ++i) {
      this->Encode(lowers[i],uppers[i]);
    }
}

void RansEncoder::Encode_Cdf(const uint32_t x, const std::vector<uint32_t> &cdf)
{
	const uint32_t lower = cdf[x], upper = cdf[x + 1];
	this->Encode(lower, upper);
}

void RansEncoder::Encode_Cdf_Batch(const std::vector<uint32_t> &x, const std::vector<std::vector<uint32_t>> &cdfs)
{
	for (int i = 0; i < static_cast<int>(x.size()); ++i) {
		const auto &cdf = cdfs[i];
		this->Encode_Cdf(x[i],cdf);
	}
}

py::bytes RansEncoder::Flush() {
  Rans64State rans;
  Rans64EncInit(&rans);

  std::vector<uint32_t> output(_syms.size(), 0xCC); // too much space ?
  uint32_t *ptr = output.data() + output.size();
  assert(ptr != nullptr);

  while (!_syms.empty()) {
    const RansSymbol sym = _syms.back();

    if (!sym.bypass) {
      Rans64EncPut(&rans, &ptr, sym.start, sym.range, precision);
    } else {
      // unlikely...
      Rans64EncPutBits(&rans, &ptr, sym.start, bypass_precision);
    }
    _syms.pop_back();
  }

  Rans64EncFlush(&rans, &ptr);

  const int nbytes =
      std::distance(ptr, output.data() + output.size()) * sizeof(uint32_t);
  return std::string(reinterpret_cast<char *>(ptr), nbytes);
}


int32_t RansDecoder::Decode(const std::vector<uint32_t> &cdf) {

    const uint32_t cum_freq = Rans64DecGet(&_rans, precision);

    // const auto cdf_end = cdf.begin() + cdfs_sizes[cdf_idx];
    const auto it = std::find_if(cdf.begin(), cdf.end(),
                                 [cum_freq](int v) { return v > cum_freq; });
    // assert(it != cdf.end() + 1);
    const uint32_t s = std::distance(cdf.begin(), it) - 1;

    Rans64DecAdvance(&_rans, &_ptr, cdf[s], cdf[s + 1] - cdf[s], precision);

    int32_t value = static_cast<int32_t>(s)+_data_min;

  return value;
}

std::vector<int32_t>
RansDecoder::Decode_Batch(const std::vector<std::vector<uint32_t>> &cdfs) {

	std::vector<int32_t> output(cdfs.size());
	for (int i = 0; i < static_cast<int>(cdfs.size()); ++i)
	{
		const auto &cdf = cdfs[i];
		output[i] = this->Decode(cdf);
	}
	return output;
}

void RansDecoder::Init(const std::string &encoded, const int32_t data_min, const int32_t data_max) {
  _stream = encoded;
  _data_min = data_min;
	_data_max = data_max;
  uint32_t *ptr = (uint32_t *)_stream.data();
  assert(ptr != nullptr);
  _ptr = ptr;
  Rans64DecInit(&_rans, &_ptr);
}


PYBIND11_MODULE(rans, m) {
  m.attr("__name__") = "rans";

  m.doc() = "range Asymmetric Numeral System python bindings";

  py::class_<RansEncoder>(m, "RansEncoder")
      .def(py::init<>())
      .def("flush", &RansEncoder::Flush)
      .def("encode", &RansEncoder::Encode)
      .def("encode_cdf", &RansEncoder::Encode_Cdf)
      .def("encode_cdf_batch", &RansEncoder::Encode_Cdf_Batch)
      .def("encode_batch", &RansEncoder::Encode_Batch);

  py::class_<RansDecoder>(m, "RansDecoder")
      .def(py::init<>())
      .def("init", &RansDecoder::Init)
      .def("decode", &RansDecoder::Decode)
      .def("decode_batch", &RansDecoder::Decode_Batch,
              "Decode a string to a list of symbols");
}
