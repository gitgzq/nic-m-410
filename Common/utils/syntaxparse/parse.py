import os
from Common.utils.syntaxparse.utils import *

# import Common.utils.iWave_utils.arithmetic_coding as ac
from Common.utils.iWave_utils.utils import (model_lambdas, qp_shifts, dec_binary, write_binary, model_qps)
import torch
from Common.utils.bee_utils.testfuncs import construct_weights, readPngToTorchIm
from PIL import Image

# from Common.models.BEE_models.ans import RansDecoder
import rans
import os

class picture():
    def __init__(self):
        self.picture_header = picture_header()
        self.slice = slice()

    def write_header_to_stream(self, f):
        f = self.picture_header.write_header_to_stream(f)

    def read_header_from_stream(self, f):
        f = self.picture_header.read_header_from_stream(f)

    def read_json_cfg(self, cfgfile, image, rate):
        import json
        with open(cfgfile) as json_file:
            data = json.load(json_file)
            picture_syntax = data["picture_syntax"]
            slice_syntax = data["slice_syntax"]
            
        self.picture_header.construct_picture_header(picture_syntax, image, rate)
        self.slice.slice_header.construct_slice_header(slice_syntax)
        
    def check_start_code(self, f):
        pass

class picture_header():
    def __init__(self):
        self.profile_id = None
        self.level_id = None
        self.model_id = None
        self.parameter_set_id = None
        self.quality_id = None
        self.picture_size_h = None
        self.picture_size_w = None
        self.output_bit_depth = None
        self.picture_format = None
        self.log2_slice_size_h_minus6 = None
        self.log2_slice_size_w_minus6 = None
        
        self.coding_tool = None
        
        self.SEI_present_flag = None

    def write_header_to_stream(self, f):
        
        # TODO: write start code
        
        code = self.profile_id <<21 | self.level_id <<15 | self.model_id <<12 | self.parameter_set_id <<5 | self.quality_id
        write_uchars(f, [(code >> 16) & 0xFF, (code >> 8) & 0xFF, code & 0xFF])
        write_uints(f, (self.picture_size_h, self.picture_size_w))
        code = self.output_bit_depth <<11 | self.picture_format <<8 | self.log2_slice_size_h_minus6 <<4 | self.log2_slice_size_w_minus6
        write_uchars(f, [(code >> 8) & 0xFF, code & 0xff])
        self.coding_tool.write_to_stream(f)
        return f

    def read_header_from_stream(self, f):
        
        # TODO: read start code
        
        bytes_read_1 = read_uchars(f, 3)
        code = bytes_read_1[0] << 16| (bytes_read_1[1] << 8) | (bytes_read_1[2])
        self.profile_id = int((code & 0xE00000) >> 21)
        self.level_id = int((code & 0x1E8000) >> 15)
        self.model_id = int((code & 0x007000) >> 12)
        self.parameter_set_id = int((code & 0x000FE0) >> 5)
        self.quality_id = code & 0x00001F
        self.picture_size_h, self.picture_size_w = read_uints(f, 2)
        
        bytes_read_2 = read_uchars(f, 2)
        code = bytes_read_2[0] << 8| (bytes_read_2[1])
        self.output_bit_depth = int((code & 0b11100000000000 ) >> 11)
        self.picture_format = int((code & 0b00011100000000 ) >> 8)
        self.log2_slice_size_h_minus6 = int((code & 0b00000011110000 ) >> 4)
        self.log2_slice_size_w_minus6 = code & 0b00000000001111
        
        # self.reversed_bit
         
        # TODO: !!!
        self.coding_tool = self.construct_coding_tool()
        # self.coding_tool.update(picture_syntax["coding_tool"])
        self.coding_tool.read_from_stream(f)
        print("Header Info:", self.profile_id, self.model_id, self.parameter_set_id)
        print("log2(slice_size) hxw:", self.log2_slice_size_h_minus6, self.log2_slice_size_w_minus6)
        return f
    
    def construct_picture_header(self, picture_syntax, image=None, rate=None):
        width, height = Image.open(image).size
        self.profile_id = picture_syntax["profile_id"]
        self.level_id = picture_syntax.get("level_id", 1)

        self.model_id = picture_syntax.get("model_id", 0)
        self.parameter_set_id = picture_syntax.get("parameter_set_id", 0)
        if self.profile_id == 0 and "parameter_set_id" not in picture_syntax:
            self.parameter_set_id = paramIDs[rate]
            paramIDs = {0.06: 0, 0.12: 3, 0.25: 7, 0.50: 10, 0.75: 15}
        self.quality_id = picture_syntax.get("quality_id", 0)
        
        self.picture_size_h = picture_syntax.get("picture_size_h", height)
        self.picture_size_w = picture_syntax.get("picture_size_w", width)
        
        self.output_bit_depth = picture_syntax.get("bit_depth", 1)  # based on WD5.1, '001' indicates 8-bit, '010' indicates 10-bit
        self.picture_format = picture_syntax.get("picture_format", 0)
        
        self.log2_slice_size_h_minus6 = picture_syntax.get("log2_slice_size_h_minus6", 0)
        self.log2_slice_size_w_minus6 = picture_syntax.get("log2_slice_size_h_minus6", 0)

        # self.slice_height = int(2**(self.log2_slice_size_h_minus6 + 6))
        # self.slice_width = int(2**(self.log2_slice_size_w_minus6 + 6))
        
        self.sei_present = picture_syntax.get("sei_present", 0)
        
        self.coding_tool = self.construct_coding_tool()
        self.coding_tool.update(picture_syntax["coding_tool"], picture_syntax)
        
    def construct_coding_tool(self, image=None, rate=None):
        if self.profile_id == 0:
            return coding_tool_0(self.model_id, image, rate)
        if self.profile_id == 1:
            return coding_tool_1(self.model_id)
        if self.profile_id == 2:
            return coding_tool_2(self.model_id)


class coding_tool_0():  # Base Profile
    def __init__(self, model_id, image, rate):
        self.model_id = model_id
        self.image = image

        self.resized_size_h = None
        self.resized_size_w = None
        self.latent_code_shape_h = None
        self.latent_code_shape_w = None
        self.output_bit_shift = 0
        self.double_precision_processing_flag = None
        self.deterministic_processing_flag = None
        self.fast_resize_flag = None
        self.num_first_level_tile = None
        self.num_second_level_tile = None
        self.num_wavefront_min = None
        self.num_wavefront_max = None
        self.waveshift = None

        self.mask_scale = mask_scale_header()
        self.adaptive_offset = adaptive_offset_header()
        self.byte_alignment = None

        # Encoder only
        self.rate = rate
        self.numIte = None

        self.first_substream_data = None
        self.second_substream_data = None

    def update(self, coding_tool_syntax):
        imagename = self.image.split("/")[-1]
        x_org, _, _ = readPngToTorchIm(self.image)
        _, _, h, w = x_org.shape
        syntax = construct_weights(coding_tool_syntax, self.rate, imagename, h, w)

        self.resized_size_h = syntax['resized_size'][0]
        self.resized_size_w = syntax['resized_size'][1]
        self.output_bit_shift = syntax['outputBitShift']
        self.double_precision_processing_flag = syntax['DoublePrecisionProcessing']
        self.deterministic_processing_flag = syntax['DeterminismSpeedup']
        self.fast_resize_flag = syntax['FastResize']
        self.num_first_level_tile = syntax['decSplit'][0]
        self.num_second_level_tile = syntax['decSplit'][1]
        self.num_wavefront_min = syntax['numthreads_min']
        self.num_wavefront_max = syntax['numthreads_max']
        self.waveshift = syntax['waveShift']
        self.mask_scale.update(syntax)
        self.adaptive_offset.update(syntax)

        # Encoder only
        self.rate = syntax['rate']
        self.numIte = syntax['numRefIte']

    def read_from_stream(self, f):
        # self.model_id, _ = parse_header(read_uchars(f, 2))
        # self.original_size_h, self.original_size_w = read_uints(f, 2)
        self.resized_size_h, self.resized_size_w = read_uints(f, 2)
        code = read_uchars(f, 1)[0]
        dummy = (code >> 4) + 1
        self.output_bit_shift = code & 0x0F
        code = read_uchars(f, 1)[0]
        self.double_precision_processing_flag = True if int((code & 0x80) >> 7) else False
        self.deterministic_processing_flag = True if int((code & 0x40) >> 6) else False
        self.fast_resize_flag = True if int((code & 0x20) >> 5) else False
        # self.postprocessing = True if int(((code & 0x20) >> 4) & 1) else False  # default 0

        self.reserved_5_bits = code & 0x1F
        f = self.mask_scale.read_header_from_stream(f)
        self.num_first_level_tile, self.num_second_level_tile = list(read_uchars(f, 2))
        f = self.adaptive_offset.read_header_from_stream(f)
        self.num_wavefront_min = read_uchars(f, 1)[0]
        self.num_wavefront_max = read_uchars(f, 1)[0]
        self.waveshift = read_uchars(f, 1)[0]
        self.latent_code_shape_h, self.latent_code_shape_w = read_uints(f, 2)

        n_strings = read_uints(f, 1)[0]
        strings = []
        for _ in range(n_strings):
            s = read_bytes(f, read_uints(f, 1)[0])
            strings.append([s])
        self.first_substream_data = strings[0]
        self.second_substream_data = strings[1]
        # return f

    def write_to_stream(self, f):
        # header = get_header(self.model_id)
        # write_uchars(f, header)
        # write_uints(f, (self.original_size_h, self.original_size_w))
        write_uints(f, (self.resized_size_h, self.resized_size_w))
        dummy = 8
        code = ((dummy - 1) << 4) | ((self.output_bit_shift) & 0x0F)
        write_uchars(f, tuple([code]))
        aa = 1 if self.double_precision_processing_flag else 0
        bb = 1 if self.deterministic_processing_flag else 0
        cc = 1 if self.fast_resize_flag else 0
        # dd = 1 if self.postprocessing else 0
        dd = 0
        code = aa << 7 | bb << 6 | cc << 5 | dd << 4  # dd is defatult 0 for postprocessing flag
        write_uchars(f, tuple([code]))
        f = self.mask_scale.write_header_to_stream(f)
        write_uchars(f, tuple([self.num_first_level_tile, self.num_second_level_tile]))
        f = self.adaptive_offset.write_header_to_stream(f)
        write_uchars(f, tuple([self.num_wavefront_min]))
        write_uchars(f, tuple([self.num_wavefront_max]))
        write_uchars(f, tuple([self.waveshift]))
        write_uints(f, (self.latent_code_shape_h, self.latent_code_shape_w))

        num_string = 2
        write_uints(f, (num_string,))
        write_uints(f, (len(self.first_substream_data[0]),))
        write_bytes(f, self.first_substream_data[0])
        write_uints(f, (len(self.second_substream_data[0]),))
        write_bytes(f, self.second_substream_data[0])
        # return f
    
class coding_tool_1(): # Main Profile
    def __init__(self, model_id):
        self.geo_flag = None
        self.multi_hyper_flag = None
        self.predictor_flag = None
        self.filtering_flag = None
        self.second_filtering_flag = None
        self.filtering_CTU = None
        self.filtering_info = None
        self.enhance_flag = None
          
    def update(self, coding_tool_syntax):
        self.geo_flag = coding_tool_syntax.get("geo_flag", 0)
        self.multi_hyper_flag = coding_tool_syntax.get("multi_hyper_flag")
        self.predictor_flag = coding_tool_syntax.get("predictor_flag")
        self.filtering_flag = coding_tool_syntax.get("filtering_flag", 0)
        self.second_filtering_flag = coding_tool_syntax.get("second_filtering_flag", 0)
    
        self.filtering_CTU = coding_tool_syntax.get("filtering_CTU")
        self.filtering_info = coding_tool_syntax.get("filtering_info")
        self.enhance_flag = coding_tool_syntax.get("enhance_flag", 0)
    
    def get_filtering_info(self):
        pass
    
    def read_from_stream(self, f):
        code = read_uchars(f, 1)[0]
        # print(code)
        self.geo_flag = int(code & 0x80) >> 7
        self.multi_hyper_flag = int(code & 0x40) >> 6
        self.predictor_flag = int(code & 0x20) >> 5
        self.filtering_flag = int(code & 0x10) >> 4
        self.second_filtering_flag = int(code & 0x08) >> 3
        self.enhance_flag = int(code & 0x04) >> 2
        # self.reserved_bit
        if self.filtering_flag:
            bytes_read = read_uchars(f, 1)
            self.filtering_CTU = int(bytes_read[0])
            self.get_filtering_info()
        # print("coding_tool_info", self.geo_flag, self.filtering_flag)
        print("\tMulti Hyper Flag:", self.multi_hyper_flag)
        print("\tPredictor Flag:", self.predictor_flag)
            
    def write_to_stream(self, f):
        # print("coding_tool_info", self.geo_flag, self.filtering_flag)
        print("\tMulti Hyper Flag:", self.multi_hyper_flag)
        print("\tPredictor Flag:", self.predictor_flag)
        code = self.geo_flag <<7 | self.multi_hyper_flag <<6 | self.predictor_flag << 5 | self.filtering_flag <<4 | self.second_filtering_flag <<3 | self.enhance_flag
        # print("[INFO] coding tool bitstream:", code)
        write_uchars(f, [code])
        if self.filtering_flag:
            write_uints(f, (self.filtering_CTU))

        # write filtering info     

class coding_tool_2(): # High Profile
    def __init__(self, model_id):

        self.isLossless = 0

        self.model_id = model_id
        self.filtering_model_id = int(0)
        self.filtering_parameter_set_id = int(0)

        self.resolution_level = 4
        self.model_index = 27
        self.block_qp_shift = 0
        self.no_estimated_delta_scale = 0

        self.model_qps = model_qps
        self.model_lambdas = model_lambdas
        self.qp_shifts = qp_shifts

        self.code_block_size = 64  # the value should be equal to 2 ** log2_block_height_minus3
        self.model_path = None

        self.img_name = None

    def read_from_stream(self, file_object):
        # bit_in = ac.BitInputStream(file_object)
        # dec = ac.ArithmeticDecoder(bit_in)

        dec = rans.RansDecoder()
        dec.init(file_object.read(),0,1000)

        if self.model_id != 2:
            self.filtering_model_id = dec_binary(dec, 3)
            self.filtering_parameter_set_id = dec_binary(dec, 7)

        self.dec = dec
        return dec
    
    def write_to_stream(self, f):


        if self.model_id != 2:
            code = int(self.filtering_model_id << 12 | self.filtering_parameter_set_id << 6)
            write_uints(f, (code,))

        return f

    def update(self, coding_tool, picture_syntax=None):

        self.isLossless = coding_tool["isLossless"]
        self.code_block_size = coding_tool["code_block_size"]

        self.qp_shift = coding_tool["qp_shift"]

        self.model_path = coding_tool["model_path"]
        self.recon_dir = coding_tool["recon_dir"]
        self.bin_dir = coding_tool["bin_dir"]
        self.log_dir = coding_tool["log_dir"]

        self.block_qp_shift = self.qp_shift

        self.filtering_model_id = coding_tool["filtering_model_id"]  #0 Use Restormer in filtering. 1. Use Restormer and GAN sequencially in filtering
        self.filtering_parameter_set_id = coding_tool["filtering_parameter_set_id"]

        if picture_syntax["parameter_set_id"] > 4.5:
            self.filtering_model_id = 1

        return

class slice():
    def __init__(self, profile_id=None):
        self.slice_header = slice_header(profile_id)
        self.slice_data = None
    
    def write_header_to_stream(self, f):
        f = self.slice_header.write_header_to_stream(f)

    def read_header_from_stream(self, f):
        f = self.slice_header.read_header_from_stream(f)

    def check_start_code(self, f):
        pass
                
class slice_header():
    def __init__(self, profile_id, geo_flag=None):
        self.profile_id = profile_id
        self.geo_flag = geo_flag
        self.slice_id = None
        self.slice_parameter_set_id_rewrite_flag = 0
        self.slice_quality_id_rewrite_flag = 0
        self.slice_parameter_set_id = None 
        self.slice_quality_id = None
        self.min_main = None
        self.max_main = None
        self.min_hyper_1 = None
        self.max_hyper_1 = None
        self.min_hyper_2 = None
        self.max_hyper_2 = None
        self.file_size_main = None
        self.file_size_hyper_1 = None
        self.file_size_hyper_2 = None
        self.geo_index = None
        self.resolution_level = None
        self.log2_subband_block_height_minus4 = None
        self.log2_subband_block_width_minus4 = None
        self.no_estimated_delta_scale = None
        
    def construct_slice_header(self, slice_syntax):
        pass
    
    def write_header_to_stream(self, f):
        
        # TODO: write slice start code
            
        write_uchars(f, tuple([self.slice_id]))
        code = self.slice_parameter_set_id_rewrite_flag << 7 | self.slice_quality_id_rewrite_flag << 6
        write_uchars(f, tuple([code]))
        if self.slice_parameter_set_id_rewrite_flag == 1:
            code = self.slice_parameter_set_id << 1
            write_uchars(f, tuple([code]))
        if self.slice_quality_id_rewrite_flag == 1:
            code = self.slice_quality_id << 3
            write_uchars(f, tuple([code]))

        if self.profile_id == 0:
            pass

        if self.profile_id == 1:
            code = (-self.min_main) << 12 | self.max_main
            write_uchars(f, [code >> 16])
            write_uchars(f, [(code & 0x00ff00) >> 8])
            write_uchars(f, [(code & 0x0000ff)])
            # write_uchars(f, tuple([code & 0x0000ff]))

            code = self.min_hyper_1 << 16 | self.max_hyper_1
            write_ints(f, tuple([code]))
            code = self.min_hyper_2 << 16 | self.max_hyper_2
            write_ints(f, tuple([code]))
            write_uints(f, (self.file_size_main, self.file_size_hyper_1, self.file_size_hyper_2))
            if self.geo_flag == 1:
                write_uchars(f, tuple([self.geo_index]))
                print("[parse (slice)] GEO Index:", self.geo_index)
        if self.profile_id == 2:
            pass    
        
        return f
        
    def read_header_from_stream(self, f, profile_id):
        
        # TODO: read slice start code
        
        self.profile_id = profile_id

        self.slice_id = read_uchars(f, 1)[0]
        code = read_uchars(f, 1)[0]
        self.slice_parameter_set_id_rewrite_flag = int(code >> 7)
        self.slice_quality_id_rewrite_flag = int(code & 0x40) >> 6
        if self.slice_parameter_set_id_rewrite_flag == 1:
            code = read_uchars(f, 1)[0]
            self.slice_quality_id = int(code >> 1)
        if self.slice_quality_id_rewrite_flag == 1:
            code = read_uchars(f, 1)[0]
            self.slice_quality_id = int(code >> 3)
        
        # ====================
        print("Profile:", self.profile_id)
        print("Parameter Reset:", self.slice_parameter_set_id_rewrite_flag)
        print("Quality Reset:", self.slice_quality_id_rewrite_flag)

        if self.profile_id == 0: pass    
        if self.profile_id == 1:
            
            self.bin_dir_path = os.path.dirname(f.name)

            code = read_uchars(f, 3)
            code = code[0] << 16 | code[1] << 8 | code[2]
            self.min_main = -int(code >> 12)
            self.max_main = int(code & 0x000fff)
            
            code = read_ints(f, 1)
            # code = code[0] << 24 | code[1] << 16 | code[2] << 8 | code[3]
            self.min_hyper_1 = code[0] >> 16
            self.max_hyper_1 = int((code[0] & 0x0000ffff))

            code = read_ints(f, 1)
            self.min_hyper_2 = code[0] >> 16
            self.max_hyper_2 = int((code[0] & 0x0000ffff))

            # self.file_size_main = read_uints(f, 1)[0]
            # self.file_size_hyper_1 = read_uints(f, 1)[0]
            # self.file_size_hyper_2 = read_uints(f, 1)[0]

            # code = self.min_hyper_1 >> 16 | self.max_hyper_1
            self.file_size_main, self.file_size_hyper_1,self.file_size_hyper_2 = read_uints(f, 3)
            if self.geo_flag == 1:
                self.geo_index = read_uchars(f, 1)[0]
                print("[parse (slice)] GEO Index:", self.geo_index)

            print("bin_dir_path:", self.bin_dir_path)

        if self.profile_id == 2: pass

        
class mask_scale_header():
    def __init__(self):
        self.num_adaptive_quant_params = None
        self.num_block_based_skip_params = None
        self.num_latent_post_process_params = None
        self.filterList = None

    def write_header_to_stream(self, f):
        def writer(num, precise):
            if precise:
                write_uints(f, [int(num * 100000)])
            else:
                write_uchars(f, [int(num * 100)])

        numfilters = [self.num_adaptive_quant_params, self.num_block_based_skip_params,
                      self.num_latent_post_process_params]
        write_uchars(f, tuple(numfilters))
        for i in range(sum(numfilters)):
            filt2Write = self.filterList[i]
            code = (filt2Write["mode"] << 4) | \
                   ((1 if filt2Write["block_size"] > 1 else 0) << 3) & 0x0F | \
                   ((1 if filt2Write["greater"] else 0) << 2) & 0x0F | \
                   ((1 if filt2Write["precise"][0] else 0) << 1) & 0x0F | \
                   ((1 if filt2Write["precise"][1] else 0)) & 0x0F

            write_uchars(f, tuple([code]))
            if filt2Write["block_size"] > 1:
                write_uchars(f, tuple([filt2Write["block_size"]]))
            writer(filt2Write["thr"], filt2Write["precise"][0])
            if filt2Write["mode"] == 5:
                writer(filt2Write["scale"][0], filt2Write["precise"][1])
                writer(filt2Write["scale"][1], filt2Write["precise"][1])
            else:
                writer(filt2Write["scale"][0], filt2Write["precise"][1])
            if filt2Write["mode"] == 4:
                write_uchars(f, tuple([len(filt2Write["channels"])]))
                if filt2Write["channels"]:
                    write_uchars(f, tuple(filt2Write["channels"]))
        return f

    def update(self, syntax):
        self.num_adaptive_quant_params, self.num_block_based_skip_params, self.num_latent_post_process_params = syntax[
            'numfilters']
        self.filterList = syntax['filterCoeffs']

    def read_header_from_stream(self, f):
        weights = list(read_uchars(f, 3))
        self.num_adaptive_quant_params, self.num_block_based_skip_params, self.num_latent_post_process_params = weights
        filter1 = []
        for i in range(sum(weights)):
            code = read_uchars(f, 1)[0]
            blkSize = (code & 0x08) + 1
            greater = True if ((code & 0x04)) else False
            precise1 = True if ((code & 0x02)) else False
            precise2 = True if ((code & 0x01)) else False
            mode = code >> 4
            if blkSize > 1:
                blkSize = read_uchars(f, 1)[0]
            thr = reader(f, precise1)
            b1 = reader(f, precise2)
            if mode == 5:
                b2 = reader(f, precise2)
                scale = [b1, b2]
            else:
                scale = [b1]
            channel_num = []
            if mode == 4:
                numFilters = read_uchars(f, 1)[0]
                if numFilters > 0:
                    channel_num = list(read_uchars(f, numFilters))
            filter = {"thr": thr, "scale": scale, "greater": greater, "mode": mode, "block_size": blkSize,
                      "channels": channel_num}
            filter1.append(filter)
            self.filterList = filter1
        return f


class adaptive_offset_header():
    def __init__(self):
        self.adaptive_offset_enabled_flag = None
        self.reserved_7_bit = None
        self.num_horizontal_split = None
        self.num_vertical_split = None
        self.numChannels4offset = 0
        self.offsetPrecision = None
        self.offset_signalled = None
        self.offsetList = None

    def write_header_to_stream(self, f):
        # TODO remove first line
        # self.offsetList = torch.ones((self.num_vertical_split, self.num_horizontal_split, 192))
        write_uchars(f, tuple([1 if self.adaptive_offset_enabled_flag else 0]))
        if self.adaptive_offset_enabled_flag:
            write_uchars(f, tuple([self.num_horizontal_split, self.num_vertical_split]))
            write_uints(f, tuple([self.offsetPrecision]))
            means_full = self.offsetList
            dummy = means_full * self.offsetPrecision ** 2

            nonZeroChannels = torch.sum(torch.sum(torch.abs((dummy).round()), dim=0), dim=0)

            _, indices = torch.sort(nonZeroChannels, descending=True)
            nonZeroChannels = nonZeroChannels.to("cpu")
            indices = indices.to("cpu")
            means_full = (means_full * self.offsetPrecision).round()
            means_full = means_full.to("cpu")
            for ind, val in enumerate(indices):
                if ind > self.numChannels4offset:
                    nonZeroChannels[val] = 0
            bytestream = []
            for i in range(0, 192, 8):
                byte = (0 if nonZeroChannels[i] == 0 else 1) << 7 | \
                       (0 if nonZeroChannels[i + 1] == 0 else 1) << 6 | \
                       (0 if nonZeroChannels[i + 2] == 0 else 1) << 5 | \
                       (0 if nonZeroChannels[i + 3] == 0 else 1) << 4 | \
                       (0 if nonZeroChannels[i + 4] == 0 else 1) << 3 | \
                       (0 if nonZeroChannels[i + 5] == 0 else 1) << 2 | \
                       (0 if nonZeroChannels[i + 6] == 0 else 1) << 1 | \
                       (0 if nonZeroChannels[i + 7] == 0 else 1)
                bytestream.append(byte)
            for x in range(self.num_vertical_split):
                for y in range(self.num_horizontal_split):
                    means = means_full[x, y, :]
                    for i in range(192):
                        if not nonZeroChannels[i] == 0:
                            sign = 1 if means[i] < 0 else 0
                            val = int(abs(means[i])) if int(abs(means[i])) < 127 else 127
                            byte = (sign << 7) | (val & 0x7F)
                            bytestream.append(byte)
            write_uchars(f, tuple(bytestream))
        return f

    def update(self, syntax):
        self.adaptive_offset_enabled_flag = syntax['channelOffsetTool']
        self.num_horizontal_split = syntax['offsetSplit_w']
        self.num_vertical_split = syntax['offsetSplit_h']
        self.offsetPrecision = syntax['offsetPrecision']
        self.offset_signalled = None
        self.numChannels4offset = syntax['numChannels4offset']
        # TODO initialized in forward function.
        self.offsetList = None

    def read_header_from_stream(self, f):
        code = read_uchars(f, 1)[0]
        self.adaptive_offset_enabled_flag = True if code else 0
        self.reserved_7_bit = (code & 0xFE) >> 1
        if self.adaptive_offset_enabled_flag:
            self.num_horizontal_split = list(read_uchars(f, 1))[0]
            self.num_vertical_split = list(read_uchars(f, 1))[0]
            self.offsetPrecision = read_uints(f, 1)[0]
            means_full = torch.zeros((self.num_vertical_split, self.num_horizontal_split, 192))
            nonZeroChannels = []
            for i in range(0, 192, 8):
                byte = read_uchars(f, 1)[0]
                nonZeroChannels.append((byte & 0x80) >> 7)
                nonZeroChannels.append((byte & 0x40) >> 6)
                nonZeroChannels.append((byte & 0x20) >> 5)
                nonZeroChannels.append((byte & 0x10) >> 4)
                nonZeroChannels.append((byte & 0x08) >> 3)
                nonZeroChannels.append((byte & 0x04) >> 2)
                nonZeroChannels.append((byte & 0x02) >> 1)
                nonZeroChannels.append(byte & 0x01)
            self.offset_signalled = nonZeroChannels
            for x in range(self.num_vertical_split):
                for y in range(self.num_horizontal_split):
                    means = torch.zeros(192)
                    for i in range(0, 192):
                        if nonZeroChannels[i] > 0:
                            byte = read_uchars(f, 1)[0]
                            sign = -1 if ((byte & 0x80) >> 7) else 1
                            val = (byte & 0x7F)
                            means[i] = sign * val
                    means_full[x, y, :] = means
            self.offsetList = means_full.permute(2, 0, 1) / self.offsetPrecision
        return f