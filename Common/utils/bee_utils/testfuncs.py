import os
import numpy as np
import cv2
import torch
import warnings
import re

torch.backends.cudnn.deterministic = True

metric_ids = {
    "mse": 0,
}


def readPngToTorchIm(inputFileName):
    bitShiftMap = {
        8: [8, 0],
        16: [10, 6],
    }
    x = cv2.imread(inputFileName, cv2.IMREAD_UNCHANGED)
    x = cv2.cvtColor(x, cv2.COLOR_BGR2RGB)
    if x.dtype == np.uint8:
        bitDepth = bitShiftMap[8][0]
        bitShift = bitShiftMap[8][1]
    elif x.dtype == np.uint16:
        bitDepth = bitShiftMap[16][0]
        bitShift = bitShiftMap[16][1]
    else:
        print("unknown input bitDepth")
        raise NotImplementedError
    x = x.astype(np.float32)
    x = torch.from_numpy(x).permute(2, 0, 1).unsqueeze(0)
    x /= (1 << bitShift)
    x /= ((1 << bitDepth) - 1)
    return x, bitDepth, bitShift


def writePngOutput(pytorchIm, bitDepth, bitshift, outFileName):
    output = outFileName
    if output is not None:
        img = pytorchIm.squeeze().permute(1, 2, 0).cpu().numpy()
        img = (img * ((1 << bitDepth) - 1)).round()
        img = img.clip(0, (1 << bitDepth) - 1)
        img *= (1 << bitshift)
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        if bitDepth <= 8:
            cv2.imwrite(output, img.astype(np.uint8))
        else:
            cv2.imwrite(output, img.astype(np.uint16))
    else:
        print("not writing output.")


def get_device(args_device):
    device = "cuda" if args_device == "cuda" and torch.cuda.is_available() else "cpu"
    if args_device == "cuda" and not torch.cuda.is_available():
        warnings.warn("Cuda is not available, running with cpu ..")
    return device


def print_args(args):
    for k, v in vars(args).items():
        print(f"{k:<15}: {v}")


def get_model_list(ckptdir):
    model_list = os.listdir(ckptdir)
    model_list = [os.path.join(ckptdir, v) for v in model_list if 'ckpt' in v and not v.startswith('.')]
    qualities = []
    for k, m in enumerate(model_list):
        r = re.search("ckpt-\d+", m).group()
        qualities.append(int(r[5:]))
    idx = np.argsort(qualities)[::-1]
    sorted_list = [model_list[v] for v in idx]
    return sorted_list


# move from Encode.testfuncs
def prepare_weights(quality, h, w):
    mean_weight_list = [0.002, 0.004, 0.006, 0.008, 0.01, 0.012]
    residual_weight_list = [0.004, 0.006, 0.008, 0.010, 0.012, 0.014]
    filterCoeffs = [
        # first set of filter coefficients control the quantization of the residual. the filters will be applied one after the other.
        {"thr": 1.30, "scale": [1.20], "greater": True, "mode": 0, "block_size": 1, "channels": [],
         "precise": [True, False]},
        {"thr": 3.98, "scale": [1.20], "greater": True, "mode": 0, "block_size": 1, "channels": [],
         "precise": [True, False]},
        {"thr": 0.95, "scale": [0.95], "greater": False, "mode": 0, "block_size": 1, "channels": [],
         "precise": [True, False]},
        # second set of filter coefficients control skipping condition of entropy coding. if a first filter OR the the second filter decides to skip a sample, that sample is skipped.
        {"thr": 0.1132, "scale": [0], "greater": True, "mode": 3, "block_size": 16, "channels": [],
         "precise": [True, False]},
        # third set of filters determine scaling at the decoder after y_hat is reconstructed. scale[0] controls weight of mean, scale[1] controls weight of residual.
        {"thr": 0.95, "scale": [mean_weight_list[6 - quality] / 1.5, residual_weight_list[6 - quality] / 1.5],
         "greater": True, "mode": 5, "block_size": 1, "channels": [], "precise": [True, True]},
        {"thr": 0.95, "scale": [mean_weight_list[6 - quality] / 3, residual_weight_list[6 - quality] / 3],
         "greater": False, "mode": 5, "block_size": 1, "channels": [], "precise": [True, True]},
    ]
    numfilters = [3, 1, 2]
    weights = {"numfilters": numfilters}
    weights['filterCoeffs'] = filterCoeffs
    weights["decSplit"] = [w // 1000 + 1, w // 400 + 1]
    weights["channelOffsetTool"] = True
    if weights["channelOffsetTool"]:
        weights["offsetSplit_w"] = w // 1600 + 1
        weights["offsetSplit_h"] = h // 600 + 1
        weights["numChannels4offset"] = 20
        weights["offsetPrecision"] = 400
    weights["numthreads_min"] = 50
    weights["numthreads_max"] = 100
    weights["waveShift"] = 1
    weights["outputBitDepth"] = 8
    weights["outputBitShift"] = 0
    weights["DoublePrecisionProcessing"] = False
    weights["DeterminismSpeedup"] = True
    weights["FastResize"] = True
    return weights


def construct_weights(data, rate, image_name, org_h=None, org_w=None, GAN=False):
    r'''
    looks into the json file. if image_name and rate matches for an entry, the recipe is loaded. 
    the recipe can be partial, missing entries are loaded as defaults.
    '''
    rate2quality = {0.75: 6, 0.5: 4, 0.25: 3, 0.12: 2, 0.06: 1}
    quality_mapped = rate2quality[rate]
    initial_weights = prepare_weights(quality_mapped, org_h, org_w)
    if org_h is not None and org_w is not None:
        initial_weights['resized_size'] = [org_h, org_w]
    initial_weights['image'] = image_name
    if GAN:
        initial_weights['model_idx'] = 2  # Tentatively, use the middle one
    initial_weights['rate'] = rate
    initial_weights['numRefIte'] = 1

    import json
    partial_weights = []
    for d in data:
        if d["image"] == image_name and d['rate'] == rate:
            partial_weights = d

    if partial_weights == []:
        print("could not find the image or rate in the encoding recipe, using initial weights")
        return initial_weights

    for entry in partial_weights.keys():
        initial_weights[entry] = partial_weights[entry]
    return initial_weights


def readPngToTorchIm(inputFileName):
    bitShiftMap = {
        8: [8, 0],
        16: [10, 6],
    }
    x = cv2.imread(inputFileName, cv2.IMREAD_UNCHANGED)
    x = cv2.cvtColor(x, cv2.COLOR_BGR2RGB)
    if x.dtype == np.uint8:
        bitDepth = bitShiftMap[8][0]
        bitShift = bitShiftMap[8][1]
    elif x.dtype == np.uint16:
        bitDepth = bitShiftMap[16][0]
        bitShift = bitShiftMap[16][1]
    else:
        print("unknown input bitDepth")
        raise NotImplementedError
    x = x.astype(np.float32)
    x = torch.from_numpy(x).permute(2, 0, 1).unsqueeze(0)
    x /= (1 << bitShift)
    x /= ((1 << bitDepth) - 1)
    return x, bitDepth, bitShift
