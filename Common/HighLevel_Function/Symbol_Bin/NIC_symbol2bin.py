import numpy as np
import torch
import torch.nn as nn

# from Common.models.NIC_models.basic_module import P_NN
# from Common.models.NIC_models.context_model import P_Model, Weighted_Gaussian_res_2mask, Weighted_Gaussian, Multistage
# from Common.models.NIC_models.factorized_entropy_model import Entropy_bottleneck
# from Common.models.NIC_models.hyper_module import h_synthesisTransform
# from Common.models.NIC_models.model import Hyper_Dec, Dec

class NIC_Symbol2Bin(nn.Module):
    def __init__(self, net, context, **kwargs):
        super().__init__()
        self.context = context
        self.net = net

    @torch.no_grad()
    def encode(self, forward_trans_varlist, out_img, picture, device):
        header, slice_header = picture.picture_header, picture.slice.slice_header
        coding_tool = header.coding_tool
        
        # params_prob = hyper_dec
        
        if coding_tool.multi_hyper_flag:
            y_main, y_hyper, y_hyper_2 = forward_trans_varlist
            # Hyper 2
            y_hyper_2_q, xp3 = self.net.factorized_entropy_func(y_hyper_2, 2)

            # Hyper 1
            tmp2 = self.net.hyper_2_dec(y_hyper_2_q)
            hyper_2_dec = self.net.p_2(tmp2)
            if coding_tool.predictor_flag:
                y_hyper_predict = self.net.Y_2(tmp2)
                y_hyper_res = y_hyper - y_hyper_predict
                y_hyper_q = torch.round(y_hyper_res)
            else:
                y_hyper_q = torch.round(y_hyper)

            # Main
            if coding_tool.predictor_flag:
                tmp = self.net.hyper_1_dec(y_hyper_q + y_hyper_predict)
            else:
                tmp = self.net.hyper_1_dec(y_hyper_q)
            hyper_dec = self.net.p(tmp)
            if coding_tool.predictor_flag:
                y_main_predict = self.net.Y(tmp)
                y_main_res = y_main - y_main_predict
                y_main_q = torch.round(y_main_res)
            else:
                y_main_q = torch.round(y_main)

            y_main_q = torch.Tensor(y_main_q.cpu().numpy().astype(np.int))
            y_hyper_2_q = torch.Tensor(y_hyper_2_q.cpu().numpy().astype(np.int))
            y_hyper_q = torch.Tensor(y_hyper_q.cpu().numpy().astype(np.int))

            y_main_q = y_main_q.to(device)
        else:
            y_main, y_hyper = forward_trans_varlist
            y_main_q = torch.round(y_main)
            y_main_q = torch.Tensor(y_main_q.cpu().numpy().astype(np.int))
            y_main_q = y_main_q.to(device)

            # y_hyper_q = torch.round(y_hyper)

            y_hyper_q, xp2 = self.net.factorized_entropy_func(y_hyper, 2)
            y_hyper_q = torch.Tensor(y_hyper_q.cpu().numpy().astype(np.int))
            y_hyper_q = y_hyper_q.to(device)
            y_hyper_2_q = None
            hyper_2_dec = None

        if coding_tool.predictor_flag:
            xp3, params_prob = self.context.compress(y_main_q, y_main_q + y_main_predict, hyper_dec,y_main_predict)
        else:
            xp3, params_prob = self.context(y_main_q, hyper_dec)
        # print("[Enc] y_hat:", y_main_q[0,:5,:5,0] + y_main_predict[0,:5,:5,0])
        #####################if use filtering, needs reconstruct the image from encoder side
        if coding_tool.filtering_flag:
            if coding_tool.predictor_flag:
                rec_img_block = self.net.decoder(y_main_q + y_main_predict, header.lambda_rd)
            else:
                rec_img_block = self.net.decoder(y_main_q, header.lambda_rd)
            if coding_tool.geo_flag:
                i_rot = slice_header.i_rot
                if header.geo_index < 4:
                    rec_img_block = torch.rot90(rec_img_block, k=4 - i_rot, dims=[2, 3])
                else:
                    rec_img_block = torch.flip(torch.rot90(rec_img_block, k=4 - i_rot, dims=[2, 3]), dims=[2])
            output_ = torch.clamp(rec_img_block, min=0., max=1.0)
            out = output_.data[0].cpu().numpy()
            out = out.transpose(1, 2, 0)

            out_img[slice_header.H_offset: slice_header.H_offset + slice_header.block_H,
            slice_header.W_offset: slice_header.W_offset + slice_header.block_W, :] = out[:slice_header.block_H, :slice_header.block_W, :]

            slice_header.W_offset += slice_header.block_W
            if slice_header.W_offset >= header.W:
                slice_header.W_offset = 0
                slice_header.H_offset += slice_header.block_H

        return y_main_q, y_hyper_q, params_prob, y_hyper_2_q, hyper_2_dec, out_img
