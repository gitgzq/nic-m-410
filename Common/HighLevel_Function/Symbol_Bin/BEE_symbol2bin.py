from Common.utils.bee_utils.tensorops import crop, update_registered_buffers
import Common.models.BEE_models.online as on
from Common.models.BEE_models.ans import BufferedRansEncoder
from Common.models.BEE_models.entropy_models import EntropyBottleneck, GaussianConditional
from Common.models.BEE_models.quantmodule import quantHSS
from Common.models.BEE_models.layers import (
    AttentionBlock,
    ResidualBlock,
    ResidualBlockUpsample,
    subpel_conv3x3,
    MaskedConv2d,
    conv,
    deconv,
)
import torch
import torch.nn as nn
import torch.nn.functional as F
import einops
import math


class BEE_Symbol2Bin(nn.Module):
    SCALES_MIN = 0.11
    SCALES_MAX = 256
    SCALES_LEVELS = 64

    def __init__(self, N=192, M=192, Quantized=True, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        self.N = int(N)
        self.M = int(M)
        self.Quantized = Quantized
        self.waveShift = 1
        self.numthreads_min = 50
        self.numthreads_max = 100
        self.numfilters = [0, 0, 0]
        self.filterCoeffs1 = []
        self.filterCoeffs2 = []
        self.filterCoeffs3 = []
        self.likely = None
        self.channelOffsetsTool = False
        self.encChannelOffsets = []
        self.offsetSplit_w = 0
        self.offsetSplit_h = 0
        self.DeterminismSpeedup = True
        self.DoublePrecisionProcessing = False
        self.encParams = None
        self.gaussian_conditional = GaussianConditional(None)
        self.entropy_bottleneck = EntropyBottleneck(self.N)
        self.y_q_full = None
        self.kernel_height = 3
        self.kernel_width = 4
        self.context_prediction = MaskedConv2d(
            M, 2 * M, kernel_size=5, padding=2, stride=1, device=device
        )
        self.entropy_parameters = nn.Sequential(
            nn.Conv2d(M * 12 // 3, M * 10 // 3, 1, device=device),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(M * 10 // 3, M * 8 // 3, 1, device=device),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(M * 8 // 3, M * 6 // 3, 1, device=device),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(M * 6 // 3, M * 5 // 3, 1, device=device),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(M * 5 // 3, M * 4 // 3, 1, device=device),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(M * 4 // 3, M * 3 // 3, 1, device=device),
        )
        self.g_s = nn.Sequential(
            ResidualBlock(N, N, device=device),
            ResidualBlockUpsample(N, N, 2, device=device),
            ResidualBlock(N, N, device=device),
            ResidualBlockUpsample(N, N, 2, device=device),
            AttentionBlock(N, device=device),
            ResidualBlock(N, N, device=device),
        )
        self.g_s_extension = nn.Sequential(
            ResidualBlockUpsample(N, N, 2, device=device),
            ResidualBlock(N, N, device=device),
            subpel_conv3x3(N, 3, 2, device=device),
        )
        self.h_s = nn.Sequential(
            deconv(N, M, stride=2, kernel_size=5, device=device),
            nn.LeakyReLU(inplace=True),
            deconv(M, M * 3 // 2, stride=2, kernel_size=5, device=device),
            nn.LeakyReLU(inplace=True),
            conv(M * 3 // 2, M * 2, stride=1, kernel_size=3, device=device),
        )
        if self.Quantized:
            self.h_s_scale = quantHSS(self.N, self.M, vbit=16, cbit=16, use_float=False, device=device)
            self.sbit = 12
        else:
            self.h_s_scale = nn.Sequential(
                deconv(N, M, stride=2, kernel_size=5, device=device),
                nn.LeakyReLU(inplace=True),
                deconv(M, M, stride=2, kernel_size=5, device=device),
                nn.LeakyReLU(inplace=True),
                conv(M, M, stride=1, kernel_size=3, device=device),
                nn.LeakyReLU(inplace=True),
                conv(M, M, stride=1, kernel_size=3, device=device),
            )

    def get_mask(self, scales_hat, filtercoeffs):
        mode = filtercoeffs["mode"]
        thr = filtercoeffs["thr"]
        block_size = filtercoeffs["block_size"]
        greater = filtercoeffs["greater"]
        scale = filtercoeffs["scale"]
        channels = filtercoeffs["channels"]
        mask = None
        self.likely = torch.abs(scales_hat)

        _, _, h, w = scales_hat.shape
        h_pad = ((h + block_size - 1) // block_size) * block_size - h
        w_pad = ((w + block_size - 1) // block_size) * block_size - w
        likely = F.pad(self.likely, (0, w_pad, 0, h_pad), value=1)
        if mode == 1:  # minpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = -(maxpool(-likely))
        if mode == 2:  # avgpool
            avgpool = torch.nn.AvgPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = avgpool(likely)
        if mode == 3:  # maxpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = (maxpool(likely))

        if greater:
            mask = (likely > thr)
        else:
            mask = (likely < thr)
        if mode == 4:  # maxpool
            for i in range(192):
                if i in channels:
                    pass
                else:
                    mask[:, i, :, :] = False
        mask = einops.repeat(mask, 'a b c d -> a b (c repeat1) (d repeat2)', repeat1=block_size, repeat2=block_size)
        mask = F.pad(mask, (0, -w_pad, 0, -h_pad), value=1)
        return mask, scale

    def HyperDecoding(self, z_hat):
        params = self.h_s(z_hat)
        return params

    def HyperScaleDecoding(self, z_hat):
        scales_hat = self.h_s_scale(z_hat)
        return scales_hat

    def _compress_ar_scale(self, y_hat, params, height, width, padding, scales_hat, forward=False):
        if ~forward:
            cdf = self.gaussian_conditional.quantized_cdf.tolist()
            cdf_lengths = self.gaussian_conditional.cdf_length.tolist()
            offsets = self.gaussian_conditional.offset.tolist()

            encoder = BufferedRansEncoder()

        count = ((height + self.numthreads_min - 1) // self.numthreads_min)
        numThreads = self.numthreads_min
        for n in range(self.numthreads_min, self.numthreads_max + 1):
            if ((height + n - 1) // n) < count:
                numThreads = n
                count = ((height + n - 1) // n)

        threadNum = range(numThreads)
        xtra = (numThreads - 1) * self.waveShift
        padDown = ((height + numThreads - 1) // numThreads) * numThreads - height
        y_q_full = torch.zeros_like(scales_hat)
        params = F.pad(params, (xtra, xtra, 0, padDown))
        y_org = y_hat.clone()
        y_hat = F.pad(y_hat, (padding + xtra, padding + xtra, padding, padding + padDown))

        batch_size, params_channel_size, _, _ = params.shape
        ctx_p_channel_shape = self.context_prediction.bias.shape
        entropy_input = torch.zeros((batch_size, int(params_channel_size) + int(ctx_p_channel_shape[0]), 1, numThreads),
                                    device=params.device)
        mask1 = []
        scale1 = []
        for i in range(self.numfilters[0]):
            mask, scale = self.get_mask(scales_hat, dict(self.filterCoeffs1[i]))
            mask = F.pad(mask, (padding + xtra, padding + xtra, padding, padding + padDown))
            mask1.append(mask)
            scale1.append(torch.tensor(scale[0]))
        torch.backends.cudnn.deterministic = False if self.DeterminismSpeedup else True

        cout, cin, _, _ = self.context_prediction.weight.shape
        ctxlayer = nn.Conv2d(cin, cout, (self.kernel_height, self.kernel_width), stride=self.kernel_width)
        ws = self.context_prediction.weight[:, :, 0:self.kernel_height, 0:self.kernel_width] * \
             self.context_prediction.mask[:, :, 0:self.kernel_height, 0:self.kernel_width]
        ctxlayer.weight = nn.Parameter(ws)
        ctxlayer.bias = nn.Parameter(self.context_prediction.bias)
        if self.DoublePrecisionProcessing:
            ctxlayer = ctxlayer.double()
            self.entropy_parameters.double()

        for h in range(0, height + padDown, numThreads):
            for w in range(width + xtra):

                params_crop = params[:, :, h:h + numThreads, w:w + numThreads]
                entropy_input[:, :params_channel_size, :, :] = einops.rearrange(params_crop,
                                                                                'a b c d ->a b (c d)').unsqueeze(2)[:,
                                                               :, :, numThreads - 1:-1:numThreads - 1]

                y_hat_crop = y_hat[:, :, h: h + numThreads + self.kernel_height - 1,
                             w: w + numThreads + self.kernel_width - 1].clone()
                if h > 0:
                    y_hat_crop[:, :, :2, :] = 0
                y_hat_crop = F.unfold(y_hat_crop, kernel_size=(self.kernel_height, self.kernel_width))
                y_hat_crop = einops.rearrange(y_hat_crop[:, :, numThreads - 1:-(numThreads - 1):numThreads - 1],
                                              'a (C r e) d->a C r (d e)', C=self.N, e=self.kernel_width,
                                              r=self.kernel_height)

                if self.DoublePrecisionProcessing:
                    y_hat_crop = y_hat_crop.double()
                    entropy_input = entropy_input.double()
                entropy_input[:, params_channel_size:, :, :] = ctxlayer(y_hat_crop)

                means_hat = self.entropy_parameters(entropy_input)
                means_hat = means_hat.float()

                y_crop = y_hat[:, :, h + padding: h + numThreads + padding, w + padding: w + numThreads + padding]
                y_crop = einops.rearrange(y_crop, 'a b c d ->a b (c d)').unsqueeze(2)[:, :, :,
                         numThreads - 1:-1:numThreads - 1]
                y_q = (y_crop - means_hat)
                mask1_crop = []
                for i in range(self.numfilters[0]):
                    mask1_crop.append(
                        mask1[i][:, :, h + padding: h + numThreads + padding, w + padding: w + numThreads + padding])
                    mask1_crop[i] = einops.rearrange(mask1_crop[i], 'a b c d ->a b (c d)').unsqueeze(2)[:, :, :,
                                    numThreads - 1:-1:numThreads - 1]
                    y_q = torch.where(mask1_crop[i], y_q * scale1[i], y_q)

                y_q = y_q.round()
                for thr in threadNum:
                    if w < (width + thr * self.waveShift) and w >= (thr * self.waveShift):
                        y_q_full[:, :, h + thr: h + 1 + thr,
                        w - thr * self.waveShift: w - thr * self.waveShift + 1] = y_q[:, :, :, thr:thr + 1]

                for i in range(self.numfilters[0] - 1, -1, -1):
                    y_q = torch.where(mask1_crop[i], y_q / scale1[i], y_q)

                wmin = max(w + padding, padding + xtra)
                wdiff = max(0, wmin - w - padding)
                wmax = min(w + padding + numThreads, width + padding + xtra)
                wdiff2 = max(w + padding + numThreads - wmax, 0)

                y_hat_crop = y_hat[:, :, h + padding: h + padding + numThreads,
                             w + padding: w + padding + numThreads].reshape(batch_size, self.N, 1, -1)
                y_hat_crop[:, :, :, numThreads - 1:-1:numThreads - 1] = means_hat + y_q
                y_hat[:, :, h + padding: h + padding + numThreads,
                w + padding + wdiff: w + padding + numThreads - wdiff2] = y_hat_crop.view(batch_size, self.N,
                                                                                          numThreads,
                                                                                          numThreads)[:, :, :,
                                                                          wdiff:numThreads - wdiff2]
        y_hat = F.pad(y_hat, (-padding - xtra, - padding - xtra, -padding, -padding - padDown))
        if forward:
            return y_hat

        self.y_hat = y_hat.clone()
        # Start of channel offsetting
        if self.channelOffsetsTool:
            wpad = ((width + self.offsetSplit_w - 1) // self.offsetSplit_w) * self.offsetSplit_w - width
            hpad = ((height + self.offsetSplit_h - 1) // self.offsetSplit_h) * self.offsetSplit_h - height
            kernel_w = (width + wpad) // self.offsetSplit_w
            kernel_h = (height + hpad) // self.offsetSplit_h
            diff = F.pad(y_org - y_hat, (0, wpad, 0, hpad))
            avg = torch.nn.AvgPool2d(kernel_size=(kernel_h, kernel_w), stride=(kernel_h, kernel_w), padding=0)
            self.encChannelOffsets = avg(diff).squeeze()
            if self.offsetSplit_h == 1:
                self.encChannelOffsets.unsqueeze_(1)
            if self.offsetSplit_w == 1:
                self.encChannelOffsets.unsqueeze_(2)
            self.encChannelOffsets = self.encChannelOffsets.permute(1, 2, 0)
        # End of channel offsetting
        torch.backends.cudnn.deterministic = True

        mask2 = torch.ones_like(scales_hat, dtype=torch.bool)
        for i in range(self.numfilters[1]):
            mask, _ = self.get_mask(scales_hat, dict(self.filterCoeffs2[i]))
            mask2 = mask2 * mask
        mask1 = []
        scale1 = []
        for i in range(self.numfilters[0]):
            mask, scale = self.get_mask(scales_hat, dict(self.filterCoeffs1[i]))
            mask1.append(mask)
            scale1.append(scale)

        for i in range(self.numfilters[0]):
            if self.Quantized:
                scales_hat = scales_hat.double()
                scales_hat[mask1[i]] *= self.h_s_scale.h_s_scale[-1].scale_v
                scales_hat[mask1[i]].round().clamp(-(2 ** (self.h_s_scale.h_s_scale[-1].abit - 1)),
                                                   2 ** (self.h_s_scale.h_s_scale[-1].abit - 1) - 1)
                scalesfactor = round(scale1[i][0] * 2 ** self.sbit)
                scales_hat[mask1[i]] *= scalesfactor
                scales_hat[mask1[i]] /= (self.h_s_scale.h_s_scale[-1].scale_v * (2 ** self.sbit))
                scales_hat = scales_hat.float()
            else:
                scales_hat[mask1[i]] *= scale1[i][0]
        self.y_q_full = y_q_full.clone()

        y_q_full = y_q_full[mask2]
        scales_hat = scales_hat[mask2]

        symbols_list = y_q_full.to('cpu').numpy()
        indexes_full = self.gaussian_conditional.build_indexes(scales_hat)
        indexes_list = indexes_full.to('cpu').numpy()

        encoder.encode_with_indexes(symbols_list, indexes_list, cdf, cdf_lengths, offsets)

        string = encoder.flush()
        return string

    def y_refiner(self, y, z, x_true_org, quality, totalIte, params, scales_hat):
        criterion = on.RateDistortionLoss(quality)
        updater = on.latentUpdater(y)
        optimizer = torch.optim.Adagrad([{'params': updater.parameters(), 'lr': 0.03}])
        _, _, h, w = x_true_org.shape
        y = updater()
        y_best = y.clone()
        y_hat = y.clone()
        loss_best = 10000000000000
        for i in range(8):
            with torch.set_grad_enabled(False):
                _, _, hh, ww = y.shape
                y_hat = self._compress_ar_scale(y.clone(), params, hh, ww, 2, scales_hat, True)

            with torch.set_grad_enabled(True):
                y_new = y - y.detach() + y_hat.detach()
                x_hat = self.g_s(y_new)
                x_hat = self.g_s_extension(x_hat)
                ctx_params = self.context_prediction(y_new)
                means_hat = self.entropy_parameters(torch.cat((params.detach(), ctx_params), dim=1))
                y_likelihoods = self.gaussian_conditional._likelihood(y, scales_hat, means_hat)
                if self.gaussian_conditional.use_likelihood_bound:
                    y_likelihoods = self.gaussian_conditional.likelihood_lower_bound(y_likelihoods)
                x_hat = crop(x_hat, (h, w))
                measurements = {"x_hat": x_hat, "likelihoods": {"y": y_likelihoods, "z": []}}
                loss = criterion(measurements, x_true_org)
                optimizer.zero_grad()
                loss["loss"].backward()
                optimizer.step()
                if loss_best > loss["loss"].item():
                    loss_best = loss["loss"].item()
                    y_best = y.clone()
                optimizer.param_groups[0]['lr'] = optimizer.param_groups[0]['lr'] / 1.2
        return y_best, z

    def Latent_refiner(self, x, y, h, w, z, yuvd, device, quality, numIte, params, scales_hat):
        x_true_org = crop(yuvd, [h, w])
        x_true_org = x_true_org.to(device)
        if x.shape[2] * x.shape[3] < 9000000:
            y, _ = self.y_refiner(y.clone(), z.clone(), x_true_org, quality, numIte, params.clone(), scales_hat.clone())
        return y

    def update(self, header):
        self.numfilters = [header.picture_header.coding_tool.mask_scale.num_adaptive_quant_params,
                           header.picture_header.coding_tool.mask_scale.num_block_based_skip_params,
                           header.picture_header.coding_tool.mask_scale.num_latent_post_process_params]
        if self.numfilters[0]:
            self.filterCoeffs1 = header.picture_header.coding_tool.mask_scale.filterList[0:self.numfilters[0]]
        if self.numfilters[1]:
            self.filterCoeffs2 = header.picture_header.coding_tool.mask_scale.filterList[
                                 self.numfilters[0]:self.numfilters[0] + self.numfilters[1]]
        if self.numfilters[2]:
            self.filterCoeffs3 = header.picture_header.coding_tool.mask_scale.filterList[
                                 self.numfilters[0] + self.numfilters[1]:]
        self.DeterminismSpeedup = header.picture_header.coding_tool.deterministic_processing_flag
        self.DoublePrecisionProcessing = header.picture_header.coding_tool.double_precision_processing_flag
        self.channelOffsetsTool = header.picture_header.coding_tool.adaptive_offset.adaptive_offset_enabled_flag
        self.decChannelOffsets = header.picture_header.coding_tool.adaptive_offset.offsetList
        self.offsetSplit_w = header.picture_header.coding_tool.adaptive_offset.num_horizontal_split
        self.offsetSplit_h = header.picture_header.coding_tool.adaptive_offset.num_vertical_split

    def entropy_update(self, scale_table=None, force=False):
        if scale_table is None:
            scale_table = self.get_scale_table()
        u1 = self.gaussian_conditional.update_scale_table(scale_table, force=force)

        u2 = False
        for m in self.children():
            if not isinstance(m, EntropyBottleneck):
                continue
            rv = m.update(force=force)
            u2 |= rv
        u1 |= u2
        return u1

    def get_scale_table(self, MIN=SCALES_MIN, MAX=SCALES_MAX, levels=SCALES_LEVELS):
        return torch.exp(torch.linspace(math.log(MIN), math.log(MAX), levels))

    def load_state_dict(self, state_dict, strict=False):
        update_registered_buffers(
            self.gaussian_conditional,
            "gaussian_conditional",
            ["_quantized_cdf", "_offset", "_cdf_length", "scale_table"],
            state_dict,
        )
        update_registered_buffers(
            self.entropy_bottleneck,
            "entropy_bottleneck",
            ["_quantized_cdf", "_offset", "_cdf_length"],
            state_dict)
        state_dict_new = state_dict.copy()
        exclude = ["g_s.6", "g_s.7", "g_s.8"]
        include = ["g_s_extension.0", "g_s_extension.1", "g_s_extension.2"]
        for k in state_dict:
            for idx, j in enumerate(exclude):
                if j in k:
                    k_new = k.replace(j, include[idx])
                    state_dict_new[k_new] = state_dict_new[k]
                    del state_dict_new[k]
        state_dict = state_dict_new
        super().load_state_dict(state_dict, strict=strict)
        self.entropy_update()

    def Decoupled_net(self, y, z, z_strings, params, z_hat, scales_hat):
        s = 4  # scaling factor between z and y
        kernel_size = 5  # context prediction kernel size
        padding = (kernel_size - 1) // 2
        height = z_hat.size(2) * s
        width = z_hat.size(3) * s
        y_hat = y.clone()
        encParams = [z, z_strings, y, y_hat[0: 0 + 1], params[0: 0 + 1], height, width, kernel_size,
                     padding, scales_hat]
        self.encParams = encParams
        z, z_strings, y, y_hat, params, height, width, kernel_size, padding, scales_hat = encParams

        count = ((height + self.numthreads_min - 1) // self.numthreads_min)
        numThreads = self.numthreads_min
        for n in range(self.numthreads_min, self.numthreads_max + 1):
            if ((height + n - 1) // n) < count:
                numThreads = n
                count = ((height + n - 1) // n)

        threadNum = range(numThreads)
        xtra = (numThreads - 1) * self.waveShift
        padDown = ((height + numThreads - 1) // numThreads) * numThreads - height
        y_q_full = torch.zeros_like(scales_hat)
        params = F.pad(params, (xtra, xtra, 0, padDown))
        y_org = y_hat.clone()
        y_hat = F.pad(y_hat, (padding + xtra, padding + xtra, padding, padding + padDown))
        batch_size, params_channel_size, _, _ = params.shape
        ctx_p_channel_shape = self.context_prediction.bias.shape
        entropy_input = torch.zeros((batch_size, int(params_channel_size) + int(ctx_p_channel_shape[0]), 1, numThreads),
                                    device=params.device)
        mask1 = []
        scale1 = []
        for i in range(self.numfilters[0]):
            mask, scale = self.get_mask(scales_hat, dict(self.filterCoeffs1[i]))
            mask = F.pad(mask, (padding + xtra, padding + xtra, padding, padding + padDown))
            mask1.append(mask)
            scale1.append(torch.tensor(scale[0]))
        torch.backends.cudnn.deterministic = False if self.DeterminismSpeedup else True

        cout, cin, _, _ = self.context_prediction.weight.shape
        ctxlayer = nn.Conv2d(cin, cout, (self.kernel_height, self.kernel_width), stride=self.kernel_width)
        ws = self.context_prediction.weight[:, :, 0:self.kernel_height, 0:self.kernel_width] * \
             self.context_prediction.mask[:, :, 0:self.kernel_height, 0:self.kernel_width]
        ctxlayer.weight = nn.Parameter(ws)
        ctxlayer.bias = nn.Parameter(self.context_prediction.bias)
        if self.DoublePrecisionProcessing:
            ctxlayer = ctxlayer.double()
            self.entropy_parameters.double()

        for h in range(0, height + padDown, numThreads):
            for w in range(width + xtra):

                params_crop = params[:, :, h:h + numThreads, w:w + numThreads]
                entropy_input[:, :params_channel_size, :, :] = einops.rearrange(params_crop,
                                                                                'a b c d ->a b (c d)').unsqueeze(2)[:,
                                                               :, :, numThreads - 1:-1:numThreads - 1]

                y_hat_crop = y_hat[:, :, h: h + numThreads + self.kernel_height - 1,
                             w: w + numThreads + self.kernel_width - 1].clone()
                if h > 0:
                    y_hat_crop[:, :, :2, :] = 0
                y_hat_crop = F.unfold(y_hat_crop, kernel_size=(self.kernel_height, self.kernel_width))
                y_hat_crop = einops.rearrange(y_hat_crop[:, :, numThreads - 1:-(numThreads - 1):numThreads - 1],
                                              'a (C r e) d->a C r (d e)', C=self.N, e=self.kernel_width,
                                              r=self.kernel_height)

                if self.DoublePrecisionProcessing:
                    y_hat_crop = y_hat_crop.double()
                    entropy_input = entropy_input.double()
                entropy_input[:, params_channel_size:, :, :] = ctxlayer(y_hat_crop)

                means_hat = self.entropy_parameters(entropy_input)
                means_hat = means_hat.float()

                y_crop = y_hat[:, :, h + padding: h + numThreads + padding, w + padding: w + numThreads + padding]
                y_crop = einops.rearrange(y_crop, 'a b c d ->a b (c d)').unsqueeze(2)[:, :, :,
                         numThreads - 1:-1:numThreads - 1]
                y_q = (y_crop - means_hat)
                mask1_crop = []
                for i in range(self.numfilters[0]):
                    mask1_crop.append(
                        mask1[i][:, :, h + padding: h + numThreads + padding, w + padding: w + numThreads + padding])
                    mask1_crop[i] = einops.rearrange(mask1_crop[i], 'a b c d ->a b (c d)').unsqueeze(2)[:, :, :,
                                    numThreads - 1:-1:numThreads - 1]
                    y_q = torch.where(mask1_crop[i], y_q * scale1[i], y_q)

                y_q = y_q.round()
                for thr in threadNum:
                    if w < (width + thr * self.waveShift) and w >= (thr * self.waveShift):
                        y_q_full[:, :, h + thr: h + 1 + thr,
                        w - thr * self.waveShift: w - thr * self.waveShift + 1] = y_q[:, :, :, thr:thr + 1]

                for i in range(self.numfilters[0] - 1, -1, -1):
                    y_q = torch.where(mask1_crop[i], y_q / scale1[i], y_q)

                wmin = max(w + padding, padding + xtra)
                wdiff = max(0, wmin - w - padding)
                wmax = min(w + padding + numThreads, width + padding + xtra)
                wdiff2 = max(w + padding + numThreads - wmax, 0)

                y_hat_crop = y_hat[:, :, h + padding: h + padding + numThreads,
                             w + padding: w + padding + numThreads].reshape(batch_size, self.N, 1, -1)
                y_hat_crop[:, :, :, numThreads - 1:-1:numThreads - 1] = means_hat + y_q
                y_hat[:, :, h + padding: h + padding + numThreads,
                w + padding + wdiff: w + padding + numThreads - wdiff2] = y_hat_crop.view(batch_size, self.N,
                                                                                          numThreads,
                                                                                          numThreads)[:, :, :,
                                                                          wdiff:numThreads - wdiff2]
        y_hat = F.pad(y_hat, (-padding - xtra, - padding - xtra, -padding, -padding - padDown))

        # if forward:
        #     return y_hat

        self.y_hat = y_hat.clone()
        # Start of channel offsetting
        if self.channelOffsetsTool:
            wpad = ((width + self.offsetSplit_w - 1) // self.offsetSplit_w) * self.offsetSplit_w - width
            hpad = ((height + self.offsetSplit_h - 1) // self.offsetSplit_h) * self.offsetSplit_h - height
            kernel_w = (width + wpad) // self.offsetSplit_w
            kernel_h = (height + hpad) // self.offsetSplit_h
            diff = F.pad(y_org - y_hat, (0, wpad, 0, hpad))
            avg = torch.nn.AvgPool2d(kernel_size=(kernel_h, kernel_w), stride=(kernel_h, kernel_w), padding=0)
            self.encChannelOffsets = avg(diff).squeeze()
            if self.offsetSplit_h == 1:
                self.encChannelOffsets.unsqueeze_(1)
            if self.offsetSplit_w == 1:
                self.encChannelOffsets.unsqueeze_(2)
            self.encChannelOffsets = self.encChannelOffsets.permute(1, 2, 0)
        # End of channel offsetting

        torch.backends.cudnn.deterministic = True

        mask2 = torch.ones_like(scales_hat, dtype=torch.bool)
        for i in range(self.numfilters[1]):
            mask, _ = self.get_mask(scales_hat, dict(self.filterCoeffs2[i]))
            mask2 = mask2 * mask
        mask1 = []
        scale1 = []
        for i in range(self.numfilters[0]):
            mask, scale = self.get_mask(scales_hat, dict(self.filterCoeffs1[i]))
            mask1.append(mask)
            scale1.append(scale)

        for i in range(self.numfilters[0]):
            if self.Quantized:
                scales_hat = scales_hat.double()
                scales_hat[mask1[i]] *= self.h_s_scale.h_s_scale[-1].scale_v
                scales_hat[mask1[i]].round().clamp(-(2 ** (self.h_s_scale.h_s_scale[-1].abit - 1)),
                                                   2 ** (self.h_s_scale.h_s_scale[-1].abit - 1) - 1)
                scalesfactor = round(scale1[i][0] * 2 ** self.sbit)
                scales_hat[mask1[i]] *= scalesfactor
                scales_hat[mask1[i]] /= (self.h_s_scale.h_s_scale[-1].scale_v * (2 ** self.sbit))
                scales_hat = scales_hat.float()
            else:
                scales_hat[mask1[i]] *= scale1[i][0]
        self.y_q_full = y_q_full.clone()
        y_q_full = y_q_full[mask2]
        scales_hat = scales_hat[mask2]
        symbols_list = y_q_full.to('cpu').detach().numpy()
        indexes_full = self.gaussian_conditional.build_indexes(scales_hat)
        indexes_list = indexes_full.to('cpu').numpy()
        return symbols_list, indexes_list

    def encode(self, forward_trans_varlist, header, device):
        rate2quality = {0.75: 6, 0.5: 4, 0.25: 3, 0.12: 2, 0.06: 1}
        x, y, z, yuvd = forward_trans_varlist[3], forward_trans_varlist[0], forward_trans_varlist[1], \
        forward_trans_varlist[2]
        rate = header.picture_header.coding_tool.rate
        numIte = header.picture_header.coding_tool.numIte
        quality_mapped = rate2quality[rate]
        torch.backends.cudnn.deterministic = True
        z_strings = self.entropy_bottleneck.compress(z)
        z_hat = self.entropy_bottleneck.decompress(z_strings, z.size()[-2:])
        params = self.HyperDecoding(z_hat)
        scales_hat = self.HyperScaleDecoding(z_hat)
        torch.backends.cudnn.deterministic = False if self.DeterminismSpeedup else True
        y = self.Latent_refiner(x, y,
                                header.picture_header.coding_tool.resized_size_h,
                                header.picture_header.coding_tool.resized_size_w,
                                z, yuvd, device, quality_mapped, numIte, params, scales_hat)
        symbols_list, indexes_list = self.Decoupled_net(y, z, z_strings, params, z_hat, scales_hat)
        header.picture_header.coding_tool.adaptive_offset.offsetList = self.encChannelOffsets
        return [symbols_list, indexes_list, forward_trans_varlist[1]]
