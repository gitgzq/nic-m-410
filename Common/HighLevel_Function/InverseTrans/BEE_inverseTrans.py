import torch.nn as nn
import torch
import torch.nn.functional as f
from Common.models.NIC_models.basic_module import Non_local_Block, ResBlock
from Common.models.BEE_models.layers import (
    AttentionBlock,
    ResidualBlock,
    ResidualBlockUpsample,
    subpel_conv3x3,
)

MeanAndResidualScale = True


class Dec(nn.Module):
    def __init__(self, input_features, N1, M, M1):
        super(Dec, self).__init__()

        self.N1 = N1
        self.M = M
        self.M1 = M1
        self.input = input_features

        self.trunk1 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1))

        self.mask1 = nn.Sequential(Non_local_Block(self.M, self.M // 2),
                                   ResBlock(self.M, self.M, 3, 1, 1),
                                   ResBlock(self.M, self.M, 3, 1, 1),
                                   ResBlock(self.M, self.M, 3, 1, 1),
                                   nn.Conv2d(self.M, self.M, 1, 1, 0))

        self.up1 = nn.ConvTranspose2d(M, M, 5, 2, 2, 1)

        self.trunk2 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1),
                                    nn.ConvTranspose2d(M, M, 5, 2, 2, 1))

        self.trunk3 = nn.Sequential(ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1),
                                    ResBlock(self.M, self.M, 3, 1, 1),
                                    nn.ConvTranspose2d(M, 2 * self.M1, 5, 2, 2, 1))

        self.trunk4 = nn.Sequential(ResBlock(2 * self.M1, 2 * self.M1, 3, 1, 1),
                                    ResBlock(2 * self.M1, 2 * self.M1, 3, 1, 1),
                                    ResBlock(2 * self.M1, 2 * self.M1, 3, 1, 1))

        self.mask2 = nn.Sequential(Non_local_Block(2 * self.M1, self.M1),
                                   ResBlock(2 * self.M1, 2 * self.M1, 3, 1, 1),
                                   ResBlock(2 * self.M1, 2 * self.M1, 3, 1, 1),
                                   ResBlock(2 * self.M1, 2 * self.M1, 3, 1, 1),
                                   nn.Conv2d(2 * self.M1, 2 * self.M1, 1, 1, 0))

        self.trunk5 = nn.Sequential(nn.ConvTranspose2d(2 * M1, M1, 5, 2, 2, 1),
                                    ResBlock(self.M1, self.M1, 3, 1, 1),
                                    ResBlock(self.M1, self.M1, 3, 1, 1),
                                    ResBlock(self.M1, self.M1, 3, 1, 1))

        self.conv1 = nn.Conv2d(self.M1, self.input, 5, 1, 2)

    def forward(self, x):
        x1 = self.trunk1(x) * f.sigmoid(self.mask1(x)) + x
        x1 = self.up1(x1)
        x2 = self.trunk2(x1)
        x3 = self.trunk3(x2)
        x4 = self.trunk4(x3) + x3
        x5 = self.trunk5(x4)
        output = self.conv1(x5)
        return output


class BEE_InverseTrans(nn.Module):
    def __init__(self, N=192, M=192, use_nic_trans=False, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") is None else kwargs.get("device")
        self.use_nic_trans = use_nic_trans
        self.N = int(N)
        self.M = int(M)
        self.DeterminismSpeedup = True
        self.decSplit1 = 1
        self.decSplit2 = 1
        if self.use_nic_trans:
            self.g_s = Dec(3, 192, 192, 96)
        else:
            self.g_s = nn.Sequential(
                ResidualBlock(N, N, device=device),
                ResidualBlockUpsample(N, N, 2, device=device),
                ResidualBlock(N, N, device=device),
                ResidualBlockUpsample(N, N, 2, device=device),
                AttentionBlock(N, device=device),
                ResidualBlock(N, N, device=device),
            )
            self.g_s_extension = nn.Sequential(
                ResidualBlockUpsample(N, N, 2, device=device),
                ResidualBlock(N, N, device=device),
                subpel_conv3x3(N, 3, 2, device=device),
            )

    @staticmethod
    def splitFunc(x, func, splitNum, pad, crop):
        _, _, _, w = x.shape
        w_step = ((w + splitNum - 1) // splitNum)
        pp = []
        for i in range(splitNum):
            start_offset = pad if i > 0 else 0
            start_crop = crop if i > 0 else 0
            end_offset = pad if i < (splitNum - 1) else 0
            end_crop = crop if i < (splitNum - 1) else 0
            dummy = func(x[:, :, :, (w_step * i) - start_offset:w_step * (i + 1) + end_offset])
            dummy = dummy[:, :, :, start_crop:]
            if end_crop > 0:
                dummy = dummy[:, :, :, :-end_crop]
            pp.append(dummy)
        x_hat = torch.cat(pp, dim=3)
        return x_hat

    def SynTrans(self, quant_latent):  # ->Tensor imArray
        if self.use_nic_trans:
            imArray = self.splitFunc(quant_latent, self.g_s, 2, 4, 64).clamp_(0, 1)
        else:
            f_map = self.DecoderFirstPart(quant_latent, self.decSplit1)
            imArray = self.DecoderSecondPart(f_map, self.decSplit2)
        return imArray

    def DecoderFirstPart(self, quant_latent, num_first_level_tile):  # -> Tensor f_map
        f_map = self.splitFunc(quant_latent, self.g_s, num_first_level_tile, 4, 16)
        return f_map

    def DecoderSecondPart(self, f_map, num_second_level_tile):  # -> Tensor imArray
        imArray = self.splitFunc(f_map, self.g_s_extension, num_second_level_tile, 4, 16).clamp_(0, 1)
        return imArray

    def decode(self, quant_latent):
        torch.cuda.empty_cache()
        torch.backends.cudnn.deterministic = False if self.DeterminismSpeedup else True
        if quant_latent.is_cuda:
            if self.use_nic_trans:
                self.g_s.half()
                quant_latent = quant_latent.half()
            else:
                self.g_s.half()
                self.g_s_extension.half()
                quant_latent = quant_latent.half()
        imArray = self.SynTrans(quant_latent)
        if quant_latent.is_cuda:
            imArray = imArray.to(torch.float32)
            self.g_s.to(torch.float32)
            if not self.use_nic_trans:
                self.g_s_extension.to(torch.float32)
        torch.backends.cudnn.deterministic = True
        return imArray

    def load_state_dict(self, state_dict, strict=False):
        state_dict_new = state_dict.copy()
        exclude = ["g_s.6", "g_s.7", "g_s.8"]
        include = ["g_s_extension.0", "g_s_extension.1", "g_s_extension.2"]
        for k in state_dict:
            for idx, j in enumerate(exclude):
                if j in k:
                    k_new = k.replace(j, include[idx])
                    state_dict_new[k_new] = state_dict_new[k]
                    del state_dict_new[k]
        state_dict = state_dict_new
        super().load_state_dict(state_dict, strict=strict)

    def update(self, header):
        self.DeterminismSpeedup = header.picture_header.coding_tool.deterministic_processing_flag
        self.decSplit1 = header.picture_header.coding_tool.num_first_level_tile
        self.decSplit2 = header.picture_header.coding_tool.num_second_level_tile
