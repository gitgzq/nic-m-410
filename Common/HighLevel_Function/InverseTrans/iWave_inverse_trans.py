import torch
import torch.nn as nn
import numpy as np

from PIL import Image
from torch.autograd import Variable
from torch.nn import functional as F

from Common.utils.iWave_utils.utils import (find_min_and_max, img2patch, img2patch_padding, yuv2rgb_lossless,
                                            patch2img,  rgb2yuv, yuv2rgb, dec_binary)

import rans

def concat(blocks_rec):
    value = torch.cat([torch.cat(blocks_h, dim=3) for blocks_h in blocks_rec], dim=2)
    return value

PRECISION = 16 
FACTOR = torch.tensor(2, dtype=torch.float32).pow_(PRECISION)
def _convert_to_int_and_normalize(cdf_float, needs_normalization=True):
    Lp = cdf_float.shape[-1]
    factor = torch.tensor(2, dtype=torch.float32, device=cdf_float.device).pow_(PRECISION)
    new_max_value = factor
    if needs_normalization:
        new_max_value = new_max_value - (Lp - 1)
    cdf_float = cdf_float.mul(new_max_value)
    cdf_float = cdf_float.round()
    cdf = cdf_float.to(dtype=torch.int32, non_blocking=True)
    if needs_normalization:
        r = torch.arange(Lp, dtype=torch.int32, device=cdf.device)
        cdf.add_(r)
    return cdf

class iWave_InverseTrans(nn.Module):
    def __init__(self, header,  **kwargs):
        super().__init__()
        self.header = header
        self.models_dict = None

    def decode(self, header, recon_path):

        if header.picture_header.coding_tool.isLossless:
            recon = self.dec_lossless(header, recon_path)
        else:
            recon = self.dec_lossy(header)

        return recon


    def dec_lossless(self, header, recon_path):

        dec = header.picture_header.dec
        freqs_resolution = 1e6

        trans_steps = header.picture_header.resolution_level
        code_block_size = 2 ** header.picture_header.log2_block_height_minus3
        split_block_height = 2 ** header.picture_header.log2_picture_height_minus6
        split_block_width = 2 ** header.picture_header.log2_picture_width_minus6

        height_all = header.picture_header.vertical_size
        width_all = header.picture_header.horizontal_size

        with torch.no_grad():
            ori_img = torch.zeros(1, 3, height_all, width_all)
            blocks_h = list(torch.split(ori_img, split_block_height, dim=2))
            blocks = [list(torch.split(block_h, split_block_width, dim=3)) for block_h in blocks_h]
            num_blocks = len(blocks)*len(blocks[0])
            print(f'Split the img into blocks, block size is {split_block_height} * {split_block_width}, num_blocks is {num_blocks}')
            
            blocks_rec = []
            block_idx = 0
            for b_h in blocks:
                
                blocks_h_rec = []
                
                for block in b_h:
                    print(f'The {block_idx} block is being decoded')
                    height = block.size()[2]
                    width = block.size()[3]
                    
                    pad_h = int(np.ceil(height / 16)) * 16 - height
                    pad_w = int(np.ceil(width / 16)) * 16 - width
                    LL = torch.zeros(3, 1, (height + pad_h) // 16, (width + pad_w) // 16).cuda()
                    HL_list = []
                    LH_list = []
                    HH_list = []
                    down_scales = [2, 4, 8, 16]
                    for i in range(trans_steps):
                        HL_list.append(torch.zeros(3, 1, (height + pad_h) // down_scales[i],
                                                (width + pad_w) // down_scales[i]).cuda())
                        LH_list.append(torch.zeros(3, 1, (height + pad_h) // down_scales[i],
                                                (width + pad_w) // down_scales[i]).cuda())
                        HH_list.append(torch.zeros(3, 1, (height + pad_h) // down_scales[i],
                                                (width + pad_w) // down_scales[i]).cuda())

                    min_v = np.zeros(shape=(3, 13), dtype=np.int)
                    max_v = np.zeros(shape=(3, 13), dtype=np.int)
                    for i in range(3):
                        for j in range(13):
                            min_v[i, j] = dec_binary(dec, 15) - 6016
                            max_v[i, j] = dec_binary(dec, 15) - 6016
                    yuv_low_bound = min_v.min(axis=0)
                    yuv_high_bound = max_v.max(axis=0)
                    shift_min = min_v - yuv_low_bound
                    shift_max = max_v - yuv_low_bound

                    subband_h = [(height + pad_h) // 2, (height + pad_h) // 4, (height + pad_h) // 8, (height + pad_h) // 16]
                    subband_w = [(width + pad_w) // 2, (width + pad_w) // 4, (width + pad_w) // 8, (width + pad_w) // 16]
                    padding_sub_h = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_h]
                    padding_sub_w = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_w]

                    # used_scale = models_dict['transform'].used_scale()

                    coded_coe_num = 0
                    # decompress LL
                    tmp_stride = subband_w[3] + padding_sub_w[3]
                    tmp_hor_num = tmp_stride // code_block_size
                    paddings = (0, padding_sub_w[3], 0, padding_sub_h[3])
                    enc_LL = F.pad(LL, paddings, "constant")
                    enc_LL = img2patch(enc_LL, code_block_size, code_block_size, code_block_size)
                    paddings = (6, 6, 6, 6)
                    enc_LL = F.pad(enc_LL, paddings, "constant")

                    temp_add_tensor = 1
                    for h_i in range(code_block_size):
                        for w_i in range(code_block_size):
                            cur_ct = enc_LL[:, :, h_i:h_i + 13, w_i:w_i + 13]

                            prob = self.models_dict['entropy_LL'].inf_cdf_lower(cur_ct, yuv_low_bound[0], yuv_high_bound[0]+1)

                            index = []

                            if temp_add_tensor == 1:
                                temp_add_tensor = 0
                                Lp_list = []
                                for p in range(3):
                                    Lp_list.append(shift_max[p, 0] + 2 - shift_min[p, 0])
                                Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                factor = FACTOR.to(prob.device)
                                new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                for p in range(3):
                                    add_tensor[p] = add_tensor[p] - shift_min[p, 0]
                                if prob.shape[0] != 3:
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                            prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                            for sample_idx, prob_sample in enumerate(prob):
                                coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                        h_i * tmp_stride + \
                                        ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                        w_i
                                if (coe_id % tmp_stride) < subband_w[3] and (coe_id // tmp_stride) < subband_h[3]:
                                    yuv_flag = sample_idx % 3
                                    if shift_min[yuv_flag, 0] < shift_max[yuv_flag, 0]:
                                        quantized_cdf = prob_sample[shift_min[yuv_flag, 0]:shift_max[yuv_flag, 0] + 2]
                                        dec_c = dec.decode(quantized_cdf) + min_v[yuv_flag, 0]
                                    else:
                                        dec_c = min_v[yuv_flag, 0]
                                    coded_coe_num = coded_coe_num + 1
                                    index.append(dec_c)
                                else:
                                    index.append(0)
                            enc_LL[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                    LL = enc_LL[:, :, 6:-6, 6:-6]
                    LL = patch2img(LL, subband_h[3] + padding_sub_h[3], subband_w[3] + padding_sub_w[3])
                    LL = LL[:, :, 0:subband_h[3], 0:subband_w[3]]
                    print('LL decoded')

                    base_context = LL

                    for i in range(trans_steps):
                        j = trans_steps - 1 - i
                        x = 3 * j + 1
                        tmp_stride = subband_w[j] + padding_sub_w[j]
                        tmp_hor_num = tmp_stride // code_block_size
                        # compress HL
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(HL_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        # context = F.pad(LL, paddings, "reflect")
                        if padding_sub_w[j] >= base_context.shape[3] or padding_sub_h[j] >= base_context.shape[2]:
                            context = F.pad(base_context, paddings, "constant")
                        else:
                            context = F.pad(base_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)

                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_HL'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = []

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)

                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            dec_c = dec.decode(quantized_cdf) + min_v[yuv_flag, x]
                                        else:
                                            dec_c = min_v[yuv_flag, x]
                                        coded_coe_num = coded_coe_num + 1
                                        index.append(dec_c)
                                    else:
                                        index.append(0)
                                enc_oth[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                        HL_list[j] = enc_oth[:, :, 6:-6, 6:-6]
                        HL_list[j] = patch2img(HL_list[j], subband_h[j] + padding_sub_h[j], subband_w[j] + padding_sub_w[j])
                        HL_list[j] = HL_list[j][:, :, 0:subband_h[j], 0:subband_w[j]]
                        print('HL' + str(j) + ' decoded')

                        # compress LH
                        x = 3 * j + 2
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(LH_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        # context = F.pad(torch.cat((LL, HL_list[j]), dim=1), paddings, "reflect")
                        temp_context = torch.cat((base_context, HL_list[j]), dim=1)
                        if padding_sub_w[j] >= temp_context.shape[3] or padding_sub_h[j] >= temp_context.shape[2]:
                            context = F.pad(temp_context, paddings, "constant")
                        else:
                            context = F.pad(temp_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)

                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_LH'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = []

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()
                                
                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            dec_c = dec.decode(quantized_cdf) + min_v[yuv_flag, x]
                                        else:
                                            dec_c = min_v[yuv_flag, x]
                                        coded_coe_num = coded_coe_num + 1
                                        index.append(dec_c)
                                    else:
                                        index.append(0)
                                enc_oth[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                        LH_list[j] = enc_oth[:, :, 6:-6, 6:-6]
                        LH_list[j] = patch2img(LH_list[j], subband_h[j] + padding_sub_h[j], subband_w[j] + padding_sub_w[j])
                        LH_list[j] = LH_list[j][:, :, 0:subband_h[j], 0:subband_w[j]]
                        print('LH' + str(j) + ' decoded')

                        # compress HH
                        x = 3 * j + 3
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(HH_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        temp_context = torch.cat((base_context, HL_list[j], LH_list[j]), dim=1)
                        if padding_sub_w[j] >= temp_context.shape[3] or padding_sub_h[j] >= temp_context.shape[2]:
                            context = F.pad(temp_context, paddings, "constant")
                        else:
                            context = F.pad(temp_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)
                        
                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_HH'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = []

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()
                                
                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            dec_c = dec.decode(quantized_cdf) + min_v[yuv_flag, x]
                                        else:
                                            dec_c = min_v[yuv_flag, x]
                                        coded_coe_num = coded_coe_num + 1
                                        index.append(dec_c)
                                    else:
                                        index.append(0)
                                enc_oth[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                        HH_list[j] = enc_oth[:, :, 6:-6, 6:-6]
                        HH_list[j] = patch2img(HH_list[j], subband_h[j] + padding_sub_h[j], subband_w[j] + padding_sub_w[j])
                        HH_list[j] = HH_list[j][:, :, 0:subband_h[j], 0:subband_w[j]]
                        print('HH' + str(j) + ' decoded')

                        base_context = torch.cat((base_context, HL_list[j], LH_list[j], HH_list[j]), dim=1)
                        base_context = F.interpolate(base_context, scale_factor=2, mode='bicubic', align_corners=False)

                    assert (coded_coe_num == (height + pad_h) * (width + pad_w) * 3)
                    #recon
                    for i in range(trans_steps):
                        j = trans_steps-i-1
                        LL = self.models_dict['transform'].inverse_trans(LL, HL_list[j], LH_list[j], HH_list[j], j)
                    recon = LL
                    recon = recon.permute(1, 0, 2, 3)
                    recon = recon[:, :, 0:height, 0:width]
                    
                    blocks_h_rec.append(recon)
                    print(f'The {block_idx} block finished')
                    block_idx += 1
                
                blocks_rec.append(blocks_h_rec)
            
            recon = concat(blocks_rec)
            recon = recon[0, :, :, :]
            recon = recon.permute(1, 2, 0)
            recon = recon.cpu().data.numpy()
            recon = yuv2rgb_lossless(recon).astype(np.float32)
            recon = np.clip(recon, 0., 255.).astype(np.uint8)
            img = Image.fromarray(recon, 'RGB')
            img.save(recon_path)

            return img

    def dec_lossy(self, header):
        dec = header.picture_header.coding_tool.dec
        freqs_resolution = 1e6

        trans_steps = header.picture_header.coding_tool.resolution_level
        code_block_size = 2 ** 6  ## TODO !! need add log2_block_height_minus3 flag in picture_header
        split_block_height = 2 ** (4 + 6)
        split_block_width = 2 ** (4 + 6)
        
        height_all = header.picture_header.picture_size_h
        width_all = header.picture_header.picture_size_w

        with torch.no_grad():
            ori_img = torch.zeros(1, 3, height_all, width_all)
            blocks_h = list(torch.split(ori_img, split_block_height, dim=2))
            blocks = [list(torch.split(block_h, split_block_width, dim=3)) for block_h in blocks_h]
            num_blocks = len(blocks)*len(blocks[0])
            print(f'Split the img into blocks, block size is {split_block_height} * {split_block_width}, num_blocks is {num_blocks}')
            
            blocks_rec = []
            block_idx = 0
            for b_h in blocks:
                
                blocks_h_rec = []
                
                for block in b_h:
                    print(f'The {block_idx} block is being decoded')
                    height = block.size()[2]
                    width = block.size()[3]
                    
                    pad_h = int(np.ceil(height / 16)) * 16 - height
                    pad_w = int(np.ceil(width / 16)) * 16 - width
                    # LL = torch.zeros(3, 1, (height + pad_h) // 16, (width + pad_w) // 16).cuda()
                    LL = torch.zeros(3, 1, (height + pad_h) // 16, (width + pad_w) // 16)#.cuda()
                    HL_list = []
                    LH_list = []
                    HH_list = []
                    down_scales = [2, 4, 8, 16]
                    for i in range(trans_steps):
                        HL_list.append(torch.zeros(3, 1, (height + pad_h) // down_scales[i],
                                                (width + pad_w) // down_scales[i]).cuda())
                        LH_list.append(torch.zeros(3, 1, (height + pad_h) // down_scales[i],
                                                (width + pad_w) // down_scales[i]).cuda())
                        HH_list.append(torch.zeros(3, 1, (height + pad_h) // down_scales[i],
                                                (width + pad_w) // down_scales[i]).cuda())

                    min_v = np.zeros(shape=(3, 13), dtype=np.int)
                    max_v = np.zeros(shape=(3, 13), dtype=np.int)
                    for i in range(3):
                        for j in range(13):
                            min_v[i, j] = dec_binary(dec, 15) - 6016
                            max_v[i, j] = dec_binary(dec, 15) - 6016
                    yuv_low_bound = min_v.min(axis=0)
                    yuv_high_bound = max_v.max(axis=0)
                    shift_min = min_v - yuv_low_bound
                    shift_max = max_v - yuv_low_bound

                    subband_h = [(height + pad_h) // 2, (height + pad_h) // 4, (height + pad_h) // 8, (height + pad_h) // 16]
                    subband_w = [(width + pad_w) // 2, (width + pad_w) // 4, (width + pad_w) // 8, (width + pad_w) // 16]
                    padding_sub_h = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_h]
                    padding_sub_w = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_w]

                    used_scale = self.models_dict['transform'].used_scale()

                    coded_coe_num = 0
                    # decompress LL
                    tmp_stride = subband_w[3] + padding_sub_w[3]
                    tmp_hor_num = tmp_stride // code_block_size
                    paddings = (0, padding_sub_w[3], 0, padding_sub_h[3])
                    enc_LL = F.pad(LL, paddings, "constant")
                    enc_LL = img2patch(enc_LL, code_block_size, code_block_size, code_block_size)
                    paddings = (6, 6, 6, 6)
                    enc_LL = F.pad(enc_LL, paddings, "constant")

                    temp_add_tensor = 1
                    for h_i in range(code_block_size):
                        for w_i in range(code_block_size):
                            cur_ct = enc_LL[:, :, h_i:h_i + 13, w_i:w_i + 13]

                            prob = self.models_dict['entropy_LL'].inf_cdf_lower(cur_ct, yuv_low_bound[0], yuv_high_bound[0]+1)

                            index = []

                            if temp_add_tensor == 1:
                                temp_add_tensor = 0
                                Lp_list = []
                                for p in range(3):
                                    Lp_list.append(shift_max[p, 0] + 2 - shift_min[p, 0])
                                Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                factor = FACTOR.to(prob.device)
                                new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                for p in range(3):
                                    add_tensor[p] = add_tensor[p] - shift_min[p, 0]
                                if prob.shape[0] != 3:
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                            prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                            for sample_idx, prob_sample in enumerate(prob):
                                coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                        h_i * tmp_stride + \
                                        ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                        w_i
                                if (coe_id % tmp_stride) < subband_w[3] and (coe_id // tmp_stride) < subband_h[3]:
                                    yuv_flag = sample_idx % 3
                                    if shift_min[yuv_flag, 0] < shift_max[yuv_flag, 0]:
                                        quantized_cdf = prob_sample[shift_min[yuv_flag, 0]:shift_max[yuv_flag, 0] + 2]
                                        dec_c = dec.decode(quantized_cdf) + min_v[yuv_flag, 0]
                                    else:
                                        dec_c = min_v[yuv_flag, 0]
                                    coded_coe_num = coded_coe_num + 1
                                    index.append(dec_c)
                                else:
                                    index.append(0)
                            enc_LL[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                    LL = enc_LL[:, :, 6:-6, 6:-6]
                    LL = patch2img(LL, subband_h[3] + padding_sub_h[3], subband_w[3] + padding_sub_w[3])
                    LL = LL[:, :, 0:subband_h[3], 0:subband_w[3]]
                    print('LL decoded')

                    LL = LL * used_scale
                    base_context = LL

                    for i in range(trans_steps):
                        j = trans_steps - 1 - i
                        x = 3 * j + 1
                        tmp_stride = subband_w[j] + padding_sub_w[j]
                        tmp_hor_num = tmp_stride // code_block_size
                        # compress HL
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(HL_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        # context = F.pad(LL, paddings, "reflect")
                        if padding_sub_w[j] >= base_context.shape[3] or padding_sub_h[j] >= base_context.shape[2]:
                            context = F.pad(base_context, paddings, "constant")
                        else:
                            context = F.pad(base_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)

                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_HL'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = []

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)

                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            dec_c = dec.decode(quantized_cdf) + min_v[yuv_flag, x]
                                        else:
                                            dec_c = min_v[yuv_flag, x]
                                        coded_coe_num = coded_coe_num + 1
                                        index.append(dec_c)
                                    else:
                                        index.append(0)
                                enc_oth[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                        HL_list[j] = enc_oth[:, :, 6:-6, 6:-6]
                        HL_list[j] = patch2img(HL_list[j], subband_h[j] + padding_sub_h[j], subband_w[j] + padding_sub_w[j])
                        HL_list[j] = HL_list[j][:, :, 0:subband_h[j], 0:subband_w[j]]
                        print('HL' + str(j) + ' decoded')

                        HL_list[j] = HL_list[j] * used_scale

                        # compress LH
                        x = 3 * j + 2
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(LH_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        # context = F.pad(torch.cat((LL, HL_list[j]), dim=1), paddings, "reflect")
                        temp_context = torch.cat((base_context, HL_list[j]), dim=1)
                        if padding_sub_w[j] >= temp_context.shape[3] or padding_sub_h[j] >= temp_context.shape[2]:
                            context = F.pad(temp_context, paddings, "constant")
                        else:
                            context = F.pad(temp_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)

                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_LH'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = []

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()
                                
                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            dec_c = dec.decode(quantized_cdf) + min_v[yuv_flag, x]
                                        else:
                                            dec_c = min_v[yuv_flag, x]
                                        coded_coe_num = coded_coe_num + 1
                                        index.append(dec_c)
                                    else:
                                        index.append(0)
                                enc_oth[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                        LH_list[j] = enc_oth[:, :, 6:-6, 6:-6]
                        LH_list[j] = patch2img(LH_list[j], subband_h[j] + padding_sub_h[j], subband_w[j] + padding_sub_w[j])
                        LH_list[j] = LH_list[j][:, :, 0:subband_h[j], 0:subband_w[j]]
                        print('LH' + str(j) + ' decoded')

                        LH_list[j] = LH_list[j] * used_scale

                        # compress HH
                        x = 3 * j + 3
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(HH_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        temp_context = torch.cat((base_context, HL_list[j], LH_list[j]), dim=1)
                        if padding_sub_w[j] >= temp_context.shape[3] or padding_sub_h[j] >= temp_context.shape[2]:
                            context = F.pad(temp_context, paddings, "constant")
                        else:
                            context = F.pad(temp_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)
                        
                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_HH'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = []

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()
                                
                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            dec_c = dec.decode(quantized_cdf) + min_v[yuv_flag, x]
                                        else:
                                            dec_c = min_v[yuv_flag, x]
                                        coded_coe_num = coded_coe_num + 1
                                        index.append(dec_c)
                                    else:
                                        index.append(0)
                                enc_oth[:, 0, h_i + 6, w_i + 6] = torch.from_numpy(np.array(index).astype(np.float)).cuda()
                        HH_list[j] = enc_oth[:, :, 6:-6, 6:-6]
                        HH_list[j] = patch2img(HH_list[j], subband_h[j] + padding_sub_h[j], subband_w[j] + padding_sub_w[j])
                        HH_list[j] = HH_list[j][:, :, 0:subband_h[j], 0:subband_w[j]]
                        print('HH' + str(j) + ' decoded')

                        HH_list[j] = HH_list[j] * used_scale

                        base_context = torch.cat((base_context, HL_list[j], LH_list[j], HH_list[j]), dim=1)
                        base_context = F.interpolate(base_context, scale_factor=2, mode='bicubic', align_corners=False)
                    
                    assert (coded_coe_num == (height + pad_h) * (width + pad_w) * 3)
                    #recon
                    for i in range(trans_steps):
                        j = trans_steps-i-1
                        LL = self.models_dict['transform'].inverse_trans(LL, HL_list[j], LH_list[j], HH_list[j], j)
                    recon = LL
                    recon = yuv2rgb(recon.permute(1, 0, 2, 3))
                    recon = recon[:, :, 0:height, 0:width]
                    
                    blocks_h_rec.append(recon)
                    print(f'The {block_idx} block finished')
                    block_idx += 1
                
                blocks_rec.append(blocks_h_rec)
            
            recon = concat(blocks_rec)

        return recon