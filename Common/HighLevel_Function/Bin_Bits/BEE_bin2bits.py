from Common.models.BEE_models.entropy_models import EntropyBottleneck, GaussianConditional
from Common.models.BEE_models.ans import BufferedRansEncoder
from Common.models.BEE_models import available_entropy_coders,set_entropy_coder
from Common.utils.bee_utils.tensorops import update_registered_buffers
import torch.nn as nn
import torch
import math
from pathlib import Path

class BEE_Bin2Bits(nn.Module):
    SCALES_MIN = 0.11
    SCALES_MAX = 256
    SCALES_LEVELS = 64

    def __init__(self,  N=192, M=192):
        super().__init__()
        self.N = int(N)
        self.M = int(M)
        self.gaussian_conditional = GaussianConditional(None)
        self.entropy_bottleneck = EntropyBottleneck(self.N)
        self.encOutput = None

    def bin2z_strings(self,z):
        z_strings = self.entropy_bottleneck.compress(z)
        return z_strings

    def bin2y_strings(self,symbols_list,indexes_list):
        y_strings = []
        cdf = self.gaussian_conditional.quantized_cdf.tolist()
        cdf_lengths = self.gaussian_conditional.cdf_length.tolist()
        offsets = self.gaussian_conditional.offset.tolist()
        encoder = BufferedRansEncoder()
        encoder.encode_with_indexes(symbols_list, indexes_list, cdf, cdf_lengths, offsets)
        string = encoder.flush()
        y_strings.append(string)
        return y_strings

    def entropy_update(self, scale_table=None, force=False):
        if scale_table is None:
            scale_table = self.get_scale_table()
        u1 = self.gaussian_conditional.update_scale_table(scale_table, force = force)

        u2 = False
        for m in self.children():
            if not isinstance(m, EntropyBottleneck):
                continue
            rv = m.update(force = force)
            u2 |= rv
        u1 |= u2
        return u1

    def get_scale_table(self, MIN=SCALES_MIN, MAX=SCALES_MAX, levels=SCALES_LEVELS):
        return torch.exp(torch.linspace(math.log(MIN), math.log(MAX), levels))

    def load_state_dict(self, state_dict, strict=False):
        update_registered_buffers(
            self.gaussian_conditional,
            "gaussian_conditional",
            ["_quantized_cdf", "_offset", "_cdf_length", "scale_table"],
            state_dict,
        )
        update_registered_buffers(
            self.entropy_bottleneck,
            "entropy_bottleneck",
            ["_quantized_cdf", "_offset", "_cdf_length"],
        state_dict)
        super().load_state_dict(state_dict, strict=strict)
        self.entropy_update()

    def encode(self, symbol2bin_varlist, picture, binName):
        symbols_list = symbol2bin_varlist[0]
        indexes_list = symbol2bin_varlist[1]
        z = symbol2bin_varlist[2]
        z_strings = self.bin2z_strings(z)
        y_strings = self.bin2y_strings(symbols_list,indexes_list)
        self.encOutput = {"strings": [y_strings, z_strings], "shape": z.size()[-2:]}
        picture.picture_header.coding_tool.latent_code_shape_h = self.encOutput["shape"][0]
        picture.picture_header.coding_tool.latent_code_shape_w = self.encOutput["shape"][1]
        picture.picture_header.coding_tool.first_substream_data = self.encOutput["strings"][0]
        picture.picture_header.coding_tool.second_substream_data = self.encOutput["strings"][1]
        f = Path(binName).open("wb")
        picture.write_header_to_stream(f)
        f.close()

    def update(self):
        set_entropy_coder(available_entropy_coders()[0])