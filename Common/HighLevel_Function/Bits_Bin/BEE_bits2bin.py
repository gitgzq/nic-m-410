import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import einops
from Common.models.BEE_models import available_entropy_coders
from Common.utils.bee_utils.tensorops import update_registered_buffers
from Common.models.BEE_models.ans import RansDecoder
from Common.models.BEE_models.entropy_models import EntropyBottleneck, GaussianConditional
from Common.models.BEE_models.quantmodule import quantHSS
from Common.models.BEE_models import set_entropy_coder
from Common.models.BEE_models.layers import (
    conv,
    deconv,
)


class BEE_Bits2Bin(nn.Module):
    SCALES_MIN = 0.11
    SCALES_MAX = 256
    SCALES_LEVELS = 64

    def __init__(self, N=192, M=192, refine=4, init_weights=True, Quantized=True, oldversion=False, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        self.M = int(M)
        self.N = int(N)
        self.Quantized = Quantized
        self.filterCoeffs1 = []
        self.filterCoeffs2 = []
        self.filterCoeffs3 = []
        self.numfilters = [0, 0, 0]
        self.gaussian_conditional = GaussianConditional(None)
        self.entropy_bottleneck = EntropyBottleneck(self.N)
        if self.Quantized:
            self.h_s_scale = quantHSS(self.N, self.M, vbit=16, cbit=16, use_float=False, device=device)
            self.sbit = 12
        else:
            self.h_s_scale = nn.Sequential(
                deconv(N, M, stride=2, kernel_size=5, device=device),
                nn.LeakyReLU(inplace=True),
                deconv(M, M, stride=2, kernel_size=5, device=device),
                nn.LeakyReLU(inplace=True),
                conv(M, M, stride=1, kernel_size=3, device=device),
                nn.LeakyReLU(inplace=True),
                conv(M, M, stride=1, kernel_size=3, device=device),
            )

    def bits2z_hat(self, string, shape, device):
        z_hat = self.entropy_bottleneck.decompress(string, shape)
        z_hat = z_hat.to(device)
        return z_hat

    def bits2w_hat(self, y_string, z_hat):
        # start: get bits2w_hat variables
        scales_hat = self.h_s_scale(z_hat)
        cdf = self.gaussian_conditional.quantized_cdf.tolist()
        cdf_lengths = self.gaussian_conditional.cdf_length.tolist()
        offsets = self.gaussian_conditional.offset.tolist()
        decoder = RansDecoder()
        decoder.set_stream(y_string[0])

        mask2 = torch.ones_like(scales_hat, dtype=torch.bool)
        for i in range(self.numfilters[1]):
            mask, _ = self.get_mask(scales_hat, dict(self.filterCoeffs2[i]))
            mask2 = mask2 * mask
        mask1 = []
        scale1 = []
        for i in range(self.numfilters[0]):
            mask, scale = self.get_mask(scales_hat, dict(self.filterCoeffs1[i]))
            mask1.append(mask)
            scale1.append(scale)
        for i in range(self.numfilters[0]):
            # Quantized scale operation
            if self.Quantized:
                scales_hat = scales_hat.double()
                scales_hat[mask1[i]] *= self.h_s_scale.h_s_scale[-1].scale_v
                scales_hat[mask1[i]].round().clamp(-(2 ** (self.h_s_scale.h_s_scale[-1].abit - 1)),
                                                   2 ** (self.h_s_scale.h_s_scale[-1].abit - 1) - 1)
                scalesfactor = round(scale1[i][0] * 2 ** self.sbit)
                scales_hat[mask1[i]] *= scalesfactor
                scales_hat[mask1[i]] /= (self.h_s_scale.h_s_scale[-1].scale_v * (2 ** self.sbit))
                scales_hat = scales_hat.float()
            else:
                scales_hat[mask1[i]] *= scale1[i][0]

        indexes_full = self.gaussian_conditional.build_indexes(scales_hat[mask2])
        indexes = indexes_full.to('cpu').numpy()
        # end
        w = decoder.decode_stream(
            indexes, cdf, cdf_lengths, offsets
        )
        w = torch.Tensor(w)
        w_full = torch.zeros_like(scales_hat)
        w_full[mask2] = w.to(z_hat.device)

        for i in range(self.numfilters[0] - 1, -1, -1):
            w_full = torch.where(mask1[i], w_full / scale1[i][0], w_full)
        return w_full, scales_hat

    def get_mask(self, scales_hat, filtercoeffs):
        mode = filtercoeffs["mode"]
        thr = filtercoeffs["thr"]
        block_size = filtercoeffs["block_size"]
        greater = filtercoeffs["greater"]
        scale = filtercoeffs["scale"]
        channels = filtercoeffs["channels"]
        mask = None
        self.likely = torch.abs(scales_hat)

        _, _, h, w = scales_hat.shape
        h_pad = ((h + block_size - 1) // block_size) * block_size - h
        w_pad = ((w + block_size - 1) // block_size) * block_size - w
        likely = F.pad(self.likely, (0, w_pad, 0, h_pad), value=1)
        if mode == 1:  # minpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = -(maxpool(-likely))
        if mode == 2:  # avgpool
            avgpool = torch.nn.AvgPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = avgpool(likely)
        if mode == 3:  # maxpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = (maxpool(likely))

        if greater:
            mask = (likely > thr)
        else:
            mask = (likely < thr)
        if mode == 4:  # maxpool
            for i in range(192):
                if i in channels:
                    pass
                else:
                    mask[:, i, :, :] = False
        mask = einops.repeat(mask, 'a b c d -> a b (c repeat1) (d repeat2)', repeat1=block_size, repeat2=block_size)
        mask = F.pad(mask, (0, -w_pad, 0, -h_pad), value=1)
        return mask, scale

    def get_scale_table(self, MIN=SCALES_MIN, MAX=SCALES_MAX, levels=SCALES_LEVELS):
        return torch.exp(torch.linspace(math.log(MIN), math.log(MAX), levels))

    def update(self, picture):
        # set_entropy_coder(available_entropy_coders()[0])
        self.numfilters = [picture.picture_header.coding_tool.mask_scale.num_adaptive_quant_params,
                           picture.picture_header.coding_tool.mask_scale.num_block_based_skip_params,
                           picture.picture_header.coding_tool.mask_scale.num_latent_post_process_params]
        if self.numfilters[0]:
            self.filterCoeffs1 = picture.picture_header.coding_tool.mask_scale.filterList[0:self.numfilters[0]]
        if self.numfilters[1]:
            self.filterCoeffs2 = picture.picture_header.coding_tool.mask_scale.filterList[
                                 self.numfilters[0]:self.numfilters[0] + self.numfilters[1]]
        if self.numfilters[2]:
            self.filterCoeffs3 = picture.picture_header.coding_tool.mask_scale.filterList[
                                 self.numfilters[0] + self.numfilters[1]:]

    def entropy_update(self, scale_table=None, force=False):
        if scale_table is None:
            scale_table = self.get_scale_table()
        u1 = self.gaussian_conditional.update_scale_table(scale_table, force=force)

        u2 = False
        for m in self.children():
            if not isinstance(m, EntropyBottleneck):
                continue
            rv = m.update(force=force)
            u2 |= rv
        u1 |= u2
        return u1

    def load_state_dict(self, state_dict, strict=False):
        update_registered_buffers(
            self.gaussian_conditional,
            "gaussian_conditional",
            ["_quantized_cdf", "_offset", "_cdf_length", "scale_table"],
            state_dict,
        )
        update_registered_buffers(
            self.entropy_bottleneck,
            "entropy_bottleneck",
            ["_quantized_cdf", "_offset", "_cdf_length"],
            state_dict)
        super().load_state_dict(state_dict, strict=strict)
        self.entropy_update()

    def decode(self, header, device):
        first_substream_data = header.picture_header.coding_tool.first_substream_data
        second_substream_data = header.picture_header.coding_tool.second_substream_data
        z_hat = self.bits2z_hat(second_substream_data, [header.picture_header.coding_tool.latent_code_shape_h,
                                                        header.picture_header.coding_tool.latent_code_shape_w], device)
        w_hat, scales_hat = self.bits2w_hat(first_substream_data, z_hat)
        return [z_hat, w_hat, scales_hat]
