import torch
import torch.nn as nn
import torch.nn.functional as F
import einops
from Common.models.BEE_models.quantmodule import quantHSS
from Common.models.BEE_models.layers import (
    AttentionBlock,
    ResidualBlock,
    ResidualBlockUpsample,
    subpel_conv3x3,
    MaskedConv2d,
    conv,
    deconv,
)

MeanAndResidualScale = True


class BEE_Bin2Symbol(nn.Module):
    def __init__(self, N=192, M=192, refine=4, init_weights=True, Quantized=False, oldversion=False, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        self.N = int(N)
        self.M = int(M)
        self.Quantized = Quantized
        self.filterCoeffs1 = []
        self.filterCoeffs2 = []
        self.filterCoeffs3 = []
        self.numfilters = [0, 0, 0]
        self.numthreads_min = 50
        self.numthreads_max = 100
        self.waveShift = 1
        self.kernel_height = 3
        self.kernel_width = 4
        self.DeterminismSpeedup = True
        self.DoublePrecisionProcessing = False
        self.channelOffsetsTool = False
        self.decChannelOffsets = []
        self.offsetSplit_w = 0
        self.offsetSplit_h = 0

        self.context_prediction = MaskedConv2d(
            M, 2 * M, kernel_size=5, padding=2, stride=1, device=device
        )
        self.entropy_parameters = nn.Sequential(
            nn.Conv2d(M * 12 // 3, M * 10 // 3, 1, device=device),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(M * 10 // 3, M * 8 // 3, 1, device=device),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(M * 8 // 3, M * 6 // 3, 1, device=device),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(M * 6 // 3, M * 5 // 3, 1, device=device),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(M * 5 // 3, M * 4 // 3, 1, device=device),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(M * 4 // 3, M * 3 // 3, 1, device=device),
        )
        self.h_s = nn.Sequential(
            deconv(N, M, stride=2, kernel_size=5, device=device),
            nn.LeakyReLU(inplace=True),
            deconv(M, M * 3 // 2, stride=2, kernel_size=5, device=device),
            nn.LeakyReLU(inplace=True),
            conv(M * 3 // 2, M * 2, stride=1, kernel_size=3, device=device),
        )
        self.g_s = nn.Sequential(
            ResidualBlock(N, N, device=device),
            ResidualBlockUpsample(N, N, 2, device=device),
            ResidualBlock(N, N, device=device),
            ResidualBlockUpsample(N, N, 2, device=device),
            AttentionBlock(N, device=device),
            ResidualBlock(N, N, device=device),
        )
        self.g_s_extension = nn.Sequential(
            ResidualBlockUpsample(N, N, 2, device=device),
            ResidualBlock(N, N, device=device),
            subpel_conv3x3(N, 3, 2, device=device),
        )
        if self.Quantized:
            self.h_s_scale = quantHSS(self.N, self.M, vbit=16, cbit=16, use_float=False, device=device)
            self.sbit = 12
        else:
            self.h_s_scale = nn.Sequential(
                deconv(N, M, stride=2, kernel_size=5, device=device),
                nn.LeakyReLU(inplace=True),
                deconv(M, M, stride=2, kernel_size=5, device=device),
                nn.LeakyReLU(inplace=True),
                conv(M, M, stride=1, kernel_size=3, device=device),
                nn.LeakyReLU(inplace=True),
                conv(M, M, stride=1, kernel_size=3, device=device),
            )

    def get_mask(self, scales_hat, filtercoeffs):
        mode = filtercoeffs["mode"]
        thr = filtercoeffs["thr"]
        block_size = filtercoeffs["block_size"]
        greater = filtercoeffs["greater"]
        scale = filtercoeffs["scale"]
        channels = filtercoeffs["channels"]
        mask = None
        self.likely = torch.abs(scales_hat)

        _, _, h, w = scales_hat.shape
        h_pad = ((h + block_size - 1) // block_size) * block_size - h
        w_pad = ((w + block_size - 1) // block_size) * block_size - w
        likely = F.pad(self.likely, (0, w_pad, 0, h_pad), value=1)
        if mode == 1:  # minpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = -(maxpool(-likely))
        if mode == 2:  # avgpool
            avgpool = torch.nn.AvgPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = avgpool(likely)
        if mode == 3:  # maxpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = (maxpool(likely))

        if greater:
            mask = (likely > thr)
        else:
            mask = (likely < thr)
        if mode == 4:  # maxpool
            for i in range(192):
                if i in channels:
                    pass
                else:
                    mask[:, i, :, :] = False
        mask = einops.repeat(mask, 'a b c d -> a b (c repeat1) (d repeat2)', repeat1=block_size, repeat2=block_size)
        mask = F.pad(mask, (0, -w_pad, 0, -h_pad), value=1)
        return mask, scale

    def LSBS(self, w_hat, quant_latent, scales_hat):  # -> Tensor quant_latent
        if MeanAndResidualScale:
            means_full = quant_latent - w_hat
            self.decParameters = [quant_latent, w_hat, scales_hat]
            for i in range(self.numfilters[2]):
                mask, scale = self.get_mask(scales_hat, dict(self.filterCoeffs3[i]))
                intermediate = quant_latent + (scale[0] * means_full + scale[1] * w_hat)
                quant_latent = torch.where(mask, intermediate, quant_latent)
        return quant_latent

    def LDAO(self, quant_latent, height, width, device):  # -> Tensor qunat_latent
        if self.channelOffsetsTool:
            wpad = ((width + self.offsetSplit_w - 1) // self.offsetSplit_w) * self.offsetSplit_w - width
            hpad = ((height + self.offsetSplit_h - 1) // self.offsetSplit_h) * self.offsetSplit_h - height
            kernel_w = (width + wpad) // self.offsetSplit_w
            kernel_h = (height + hpad) // self.offsetSplit_h
            self.decChannelOffsets = self.decChannelOffsets.to(device).unsqueeze(0)
            self.decChannelOffsets = einops.repeat(self.decChannelOffsets, 'a b c d -> a b (c repeat1) (d repeat2)',
                                                   repeat1=kernel_h, repeat2=kernel_w)
            self.decChannelOffsets = F.pad(self.decChannelOffsets, (0, -wpad, 0, -hpad))
            quant_latent += self.decChannelOffsets
        return quant_latent

    def HyperDecoding(self, z_hat):  # ->FeatureMap1
        FeatureMap1 = self.h_s(z_hat)
        return FeatureMap1

    def Decouple_net(self, w_hat, featureMap1, height, width, padding, scales_hat, device):
        count = ((height + self.numthreads_min - 1) // self.numthreads_min)
        numThreads = self.numthreads_min
        for n in range(self.numthreads_min, self.numthreads_max + 1):
            if ((height + n - 1) // n) < count:
                numThreads = n
                count = ((height + n - 1) // n)
        xtra = (numThreads - 1) * self.waveShift
        padDown = ((height + numThreads - 1) // numThreads) * numThreads - height
        numTiles = (height + padDown) // numThreads

        y_hat = torch.zeros(
            (scales_hat.size(0),
             self.M,
             height + 2 * padding + padDown + (numTiles - 1) * (self.kernel_height - 1),
             width + 2 * (padding + xtra)),
            device=scales_hat.device,
        )

        featureMap1 = F.pad(featureMap1, (xtra, xtra, 0, padDown + (numTiles - 1) * (self.kernel_height - 1)))
        w_hat = F.pad(w_hat, (xtra, xtra, 0, padDown + (numTiles - 1) * (self.kernel_height - 1)))

        torch.backends.cudnn.deterministic = False if self.DeterminismSpeedup else True

        _, params_channel_size, _, _ = featureMap1.shape
        ctx_p_channel_shape = self.context_prediction.bias.shape
        entropy_input = torch.zeros((1,
                                     int(params_channel_size) + int(ctx_p_channel_shape[0]),
                                     numThreads * numTiles + (numTiles - 1) * (self.kernel_height - 1),
                                     1),
                                    device=device)

        cout, cin, _, _ = self.context_prediction.weight.shape
        ctxlayer = nn.Conv2d(cin, cout, (self.kernel_height, self.kernel_width), stride=(1, self.kernel_width))
        ws = self.context_prediction.weight[:, :, 0:self.kernel_height,
             0:self.kernel_width] * self.context_prediction.mask[:, :, 0:self.kernel_height, 0:self.kernel_width]
        ws_new = torch.zeros_like(ws)

        for i in range(3):
            ws_new[:, :, i, i:] = ws[:, :, i, :self.kernel_width - i]
        ctxlayer.weight = nn.Parameter(ws_new)
        ctxlayer.bias = nn.Parameter(self.context_prediction.bias)
        if self.DoublePrecisionProcessing:
            ctxlayer = ctxlayer.double()
            self.entropy_parameters.double()

        rv_new = torch.zeros_like(w_hat)
        params_new = torch.zeros_like(featureMap1)
        _, _, _, params_crop_with = featureMap1.shape

        # 1. prepare the data
        for index, h in enumerate(range(0, height + padDown, numThreads)):
            for j in range(numThreads):
                h_index = h + j
                rv_new[:, :, h_index + 2 * index, 0:params_crop_with - numThreads + 1] = w_hat[:, :, h_index,
                                                                                         numThreads - 1 - j:params_crop_with - j]
                params_new[:, :, h_index + 2 * index, 0:params_crop_with - numThreads + 1] = featureMap1[:, :, h_index,
                                                                                             numThreads - 1 - j:params_crop_with - j]

        # 2. prediction with regular neural layers
        for w in range(width + xtra):
            y_hat_crop = y_hat[:, :, :numTiles * numThreads + numTiles * (self.kernel_height - 1),
                         w: w + self.kernel_width]
            entropy_input[:, :params_channel_size, :, :] = params_new[:, :, :, w:w + 1]

            if self.DoublePrecisionProcessing:
                y_hat_crop = y_hat_crop.double()
                entropy_input = entropy_input.double()
            entropy_input[:, params_channel_size:, :, :] = ctxlayer(y_hat_crop)
            means_hat = self.entropy_parameters(entropy_input)
            if self.DoublePrecisionProcessing:
                means_hat = means_hat.float()

            y_hat2 = rv_new[:, :, :, w:w + 1] + means_hat

            wmin = max(w + padding, padding + xtra)
            wdiff = max(0, wmin - w - padding)
            wmax = min(w + padding + numThreads, width + padding + xtra)
            wdiff2 = max(w + padding + numThreads - wmax, 0)
            for index, h in enumerate(range(0, height + padDown, numThreads)):
                y_hat[:, :, wdiff2 + h + 2 * (index + 1): h + numThreads + 2 * (index + 1) - wdiff,
                w + 4: w + 5] = y_hat2[:, :, 2 * index + h + wdiff2: 2 * index + h + numThreads - wdiff, :]

        # 3. converting the y_hat to initial shape
        for index, h in enumerate(range(0, height + padDown, numThreads)):
            for j in range(numThreads):
                h_index = h + j
                y_hat[:, :, 2 + h_index, 0:-2 * (xtra + padding)] = y_hat[:, :, 2 * (index + 1) + h_index,
                                                                    4 + j: -2 * (xtra + padding) + j + 4]

        y_hat = F.pad(y_hat, (0, 2 * (-padding - xtra), -padding, -padding - padDown - (numTiles - 1) * (self.kernel_height - 1)))
        self.entropy_parameters.float()
        return y_hat

    def update(self, picture):
        self.numfilters = [picture.picture_header.coding_tool.mask_scale.num_adaptive_quant_params,
                           picture.picture_header.coding_tool.mask_scale.num_block_based_skip_params,
                           picture.picture_header.coding_tool.mask_scale.num_latent_post_process_params]
        if self.numfilters[0]:
            self.filterCoeffs1 = picture.picture_header.coding_tool.mask_scale.filterList[0:self.numfilters[0]]
        if self.numfilters[1]:
            self.filterCoeffs2 = picture.picture_header.coding_tool.mask_scale.filterList[
                                 self.numfilters[0]:self.numfilters[0] + self.numfilters[1]]
        if self.numfilters[2]:
            self.filterCoeffs3 = picture.picture_header.coding_tool.mask_scale.filterList[
                                 self.numfilters[0] + self.numfilters[1]:]
        self.DeterminismSpeedup = picture.picture_header.coding_tool.deterministic_processing_flag
        self.DoublePrecisionProcessing = picture.picture_header.coding_tool.double_precision_processing_flag
        self.channelOffsetsTool = picture.picture_header.coding_tool.adaptive_offset.adaptive_offset_enabled_flag
        self.decChannelOffsets = picture.picture_header.coding_tool.adaptive_offset.offsetList
        self.offsetSplit_w = picture.picture_header.coding_tool.adaptive_offset.num_horizontal_split
        self.offsetSplit_h = picture.picture_header.coding_tool.adaptive_offset.num_vertical_split

    def decode(self, varlist, device):
        z_hat = varlist[0]
        w_hat = varlist[1]
        scales_hat = varlist[2]

        s = 4
        kernel_size = 5
        padding = (kernel_size - 1) // 2
        height = z_hat.size(2) * s
        width = z_hat.size(3) * s

        featureMap1 = self.HyperDecoding(z_hat)
        quant_latent = self.Decouple_net(w_hat, featureMap1, height, width, padding, scales_hat, device)
        quant_latent = self.LDAO(quant_latent, height, width, device)
        quant_latent = self.LSBS(w_hat, quant_latent, scales_hat)
        return quant_latent
