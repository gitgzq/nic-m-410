import struct
import numpy as np
import torch
import torch.nn as nn
from PIL import Image
from Common.utils.nic_utils.block_metric import check_RD_GEO # slice based metric
from Common.utils.nic_utils.generate_substitute import SubstituteGenerator
import Common.models.NIC_models.model as model
from Common.models.NIC_models.context_model import Weighted_Gaussian, Weighted_Gaussian_res_2mask, Multistage

lambdas_list = [0.00105, 0.0042, 0.0160, 0.0618, 0.2950,  ##mse
                1.40, 6.50, 27.00, 115.37, 440.00]  ##ms-ssim

class NIC_Preprocess(nn.Module):
    def __init__(self, header):
        super().__init__()
        model_index, lambda_rd_ori = header.parameter_set_id, header.quality_id
        coding_tool = header.coding_tool

        header.USE_VR_MODEL = 0
        header.C = 3
        M, N2 = 192, 128 # VR_Low
        
        if header.model_id < 2:
            header.USE_VR_MODEL = 1
            if header.model_id == 0 and header.parameter_set_id < 2:
                header.reconstruction_metric = 'mse'
            if header.model_id == 1 and header.parameter_set_id == 0:
                header.reconstruction_metric = 'msssim'
            header.lmbda = lambda_rd_ori * 100
            
            if header.model_id == 0:
                header.max_lambdas = [6, 0.08]
            if header.model_id == 1:
                header.max_lambdas = [44, 296, 0.96, 7.68]
                # self.picture.picture_header.max_lambdas = [44, 296, 0.96, 7.68]
            
            lambda_rd_max = header.max_lambdas[model_index]

            if lambda_rd_ori > 1.2 * lambda_rd_max:
                lambda_rd_ori = 1.2 * lambda_rd_max
            lambda_rd_nom = lambda_rd_ori / lambda_rd_max
            header.lambda_rd_nom_scaled = int(lambda_rd_nom / 1.2 * pow(2, 16))
            lambda_rd_nom_used = header.lambda_rd_nom_scaled / pow(2, 16) * 1.2
            lambda_rd_numpy = np.zeros((1, 1), np.float32)
            lambda_rd_numpy[0, 0] = lambda_rd_nom_used
            header.lambda_rd = torch.Tensor(lambda_rd_numpy)
            
            if (model_index in [1,2]) or (model_index in [4,5]): # VR_High
                M, N2 = 256, 192
            
            if coding_tool.multi_hyper_flag:
                if coding_tool.predictor_flag:
                    self.image_comp = model.Image_coding_multi_hyper_res(3, M, N2, M, M // 2, header.USE_VR_MODEL)
                else:
                    self.image_comp = model.Image_coding_multi_hyper(3, M, N2, M, M // 2, header.USE_VR_MODEL)
            else:
                self.image_comp = model.Image_coding(3, M, N2, M, M // 2, header.USE_VR_MODEL)
            if coding_tool.predictor_flag:
                self.context = Multistage(M)
            else:
                self.context = Weighted_Gaussian(M)
        else:
            header.reconstruction_metric = 'mse' if model_index <= 4 else 'msssim'
            header.lmbda = lambdas_list[model_index]
            # if (model_index == 6) or (model_index == 7) or (model_index == 14) or (model_index == 15):
            #     M, N2 = 256, 192
            if coding_tool.multi_hyper_flag:
                if coding_tool.predictor_flag:
                    self.image_comp = model.Image_coding_multi_hyper_res(3, M, N2, M, M // 2, header.USE_VR_MODEL)
                else:
                    self.image_comp = model.Image_coding_multi_hyper(3, M, N2, M, M // 2, header.USE_VR_MODEL)
            else:
                self.image_comp = model.Image_coding(3, M, N2, M, M // 2, header.USE_VR_MODEL)
            if coding_tool.predictor_flag:
                self.context = Multistage(M)
            else:
                self.context = Weighted_Gaussian(M)
            header.lambda_rd = None

        if coding_tool.filtering_flag:
            # lmbda_list = [200, 400, 800, 1600, 3200, 6400, 12800, 25600, 4, 8, 16, 32, 64, 128, 320, 640]
            # stepsize_list = [150, 75, 30, 10, 10, 5, 3, 1, 100, 10, 7, 5, 1, 1, 1, 0.3]
            #Todo: This part is different from NIC-v0.5
            stepsize_list = [150, 75, 30, 10, 10, 100, 10, 7, 5, 1]
            if header.model_id < 2:
                # header.reconstruction_metric = 'mse' if model_index <= 2 else 'msssim'
                # header.lmbda = lambda_rd_ori * 100
                step_size = self.get_step(header.lmbda, header.reconstruction_metric)
                print(step_size)
            else:
                step_size = stepsize_list[model_index]
                # header.lmbda = lmbda_list[model_index]
                # header.reconstruction_metric = 'mse' if model_index <= 7 else 'msssim'
            
            self.substitute_generator = SubstituteGenerator(model=self.image_comp, context_model=self.context, llambda=header.lmbda,
                                           num_steps=header.num_steps, step_size=step_size,
                                           reconstruct_metric=header.reconstruction_metric,
                                           )
        header.M, header.N2 = M, N2

    @staticmethod
    def get_step(lamb, metric):
        if metric == 'mse':
            lmbda_list = [40, 200, 400, 800, 1600, 3200, 6400, 12800, 25600, 29600]
            stepsize_list = [200, 150, 75, 30, 10, 10, 5, 3, 1, 1]
        elif metric == 'msssim':
            lmbda_list = [0.5, 4, 8, 16, 32, 64, 128, 320, 640, 768]
            stepsize_list = [150, 100, 10, 7, 5, 1, 1, 1, 0.3, 0.3]
        else:
            pass

        floor = np.array(lmbda_list)[:-1].astype(np.float32)
        floor[floor>lamb] = 0
        ceil = np.array(lmbda_list)[1:].astype(np.float32)
        ceil[ceil<lamb] = 0
        index = np.nonzero(floor*ceil)[0][0]
        range = lmbda_list[index+1] - lmbda_list[index]
        alpha1 = (lamb - lmbda_list[index])/range
        alpha2 = (lmbda_list[index+1] - lamb)/range
        return alpha1*stepsize_list[index+1]+alpha2*stepsize_list[index]

    def encode(self, image, picture, device):
        header, slice_header = picture.picture_header, picture.slice.slice_header
        img = Image.open(image)
        source_img = np.array(img)
        H, W, C_ = source_img.shape
        header.picture_size_h, header.picture_size_w = H, W
        if C_ > 3:
            source_img = source_img[:,:,:3]
            print("[WARNING]: Input image has 4 channels! only first 3 channels are encoded.")
        C = 3
        img = source_img / 255.0
        # num_pixels = H * W
        # block_header.block_width = header.CTU_size
        # block_header.block_height = header.CTU_size
        slice_header.H_offset = 0
        slice_header.W_offset = 0

        slice_header.height = int(2 ** (header.log2_slice_size_h_minus6 + 6))
        slice_header.width = int(2 ** (header.log2_slice_size_w_minus6 + 6))
        header.Slice_Num_in_Height = int(np.ceil(H / slice_header.height))
        header.Slice_Num_in_Width = int(np.ceil(W / slice_header.width))
        print("Slice Size:", slice_header.height, slice_header.width)
        # write head info to file
        # if header.reconstruction_metric == 'msssim':
            # Head = struct.pack('2HB6?H', H, W, header.model_index, header.USE_GEO, header.model_id < 2, header.USE_MULTI_HYPER, header.USE_PREDICTOR,
                        #    False, header.USE_ACCELERATION, header.CTU_size)
            
        # else:
            # Head = struct.pack('2HB6?HB', H, W, header.model_index, header.USE_GEO, header.model_id < 2, header.USE_MULTI_HYPER, header.USE_PREDICTOR, header.USE_POSTPROCESSING, header.USE_ACCELERATION, header.CTU_size, header.USE_SECOND_POSTPROCESSING)
        # file_object.write(Head)

        # if header.filtering_flag and header.reconstruction_metric == 'mse':
        #     Head_post_CTU = struct.pack('H', header.filtering_CTU)
        #     file_object.write(Head_post_CTU)

        # if header.model_id < 2:
        #     Head_lmbda = struct.pack('H', header.lambda_rd_nom_scaled)
        #     file_object.write(Head_lmbda)

        ######################### spliting Image #########################
        img_slice_list = []
        for i in range(header.Slice_Num_in_Height):
            for j in range(header.Slice_Num_in_Width):
                img_slice_list.append(img[i * slice_header.height:np.minimum((i + 1) * slice_header.height, H),
                                    j * slice_header.width:np.minimum((j + 1) * slice_header.width, W), ...])

        header.H, header.W, header.C = H, W, C
        return source_img, img_slice_list

    def process_slice(self, im_slice, picture):
        ############################ Geometric Flip and rotate ########################
        if picture.picture_header.coding_tool.geo_flag:
            _, _, picture.slice.slice_header.geo_index, _, picture.slice.slice_header.orig_rd = check_RD_GEO(im_slice, self.image_comp, self.context, picture.picture_header)
            
            picture.slice.slice_header.i_rot = int(picture.slice.slice_header.geo_index % 4)
            if picture.slice.slice_header.geo_index < 4:
                im_slice = torch.rot90(im_slice, k=picture.slice.slice_header.i_rot, dims=[2, 3])
            else:
                im_slice = torch.rot90(torch.flip(im_slice, dims=[2]), k=picture.slice.slice_header.i_rot, dims=[2, 3])
        # print("[Preprocess] GEO index:", picture.slice.slice_header.geo_index)
        ############################ Preprocessing to find a substitute ########################
        if picture.picture_header.coding_tool.filtering_flag:
            if picture.picture_header.model_id < 2:
                im_slice = self.substitute_generator.perturb(orig_image=im_slice, orig_rd = picture.slice.slice_header.orig_rd, lambda_rd=picture.picture_header.lambda_rd)
            else:
                im_slice = self.substitute_generator.perturb(orig_image=im_slice, orig_rd = picture.slice.slice_header.orig_rd,)
        return im_slice

