import copy
import os
import time

import numpy as np
import torch
from PIL import Image
from torch.autograd import Variable
import torch.nn as nn
from torch.nn import functional as F
import struct

import Common.models.iWave_models.Model as Model
from Common.utils.iWave_utils.utils import (find_min_and_max, img2patch, img2patch_padding, yuv2rgb_lossless, patch2img,
                                            rgb2yuv, yuv2rgb, rgb2yuv_lossless, write_binary, to_variable, qp_shifts,
                                            model_lambdas, model_qps)
from Common.utils.iWave_utils.metric.metrics import evaluate

import rans

def concat(blocks_rec):
    value = torch.cat([torch.cat(blocks_h, dim=3) for blocks_h in blocks_rec], dim=2)
    return value

def write_uchars(fd, values, fmt=">{:d}B"):
    fd.write(struct.pack(fmt.format(len(values)), *values))
    
def read_uchars(fd, n, fmt=">{:d}B"):
    sz = struct.calcsize("B")
    return struct.unpack(fmt.format(n), fd.read(n * sz))

PRECISION = 16 
FACTOR = torch.tensor(2, dtype=torch.float32).pow_(PRECISION)
def _convert_to_int_and_normalize(cdf_float, needs_normalization=True):
    Lp = cdf_float.shape[-1]
    factor = torch.tensor(2, dtype=torch.float32, device=cdf_float.device).pow_(PRECISION)
    new_max_value = factor
    if needs_normalization:
        new_max_value = new_max_value - (Lp - 1)
    cdf_float = cdf_float.mul(new_max_value)
    cdf_float = cdf_float.round()
    cdf = cdf_float.to(dtype=torch.int32, non_blocking=True)
    if needs_normalization:
        r = torch.arange(Lp, dtype=torch.int32, device=cdf.device)
        cdf.add_(r)
    return cdf


class iWave_ForwardTrans(nn.Module):
    def __init__(self, header,  **kwargs):
        super().__init__()
        self.header = header
        self.models_dict = None

    def encode(self, image, header):
        header.img_name = image
        self.header = header

        if header.coding_tool.isLossless == 0:
            self.enc_lossy(header)
        else:
            self.enc_lossless(header, header.coding_tool.recon_dir)

    def enc_lossless(self, header, recon_path):
        assert header.isLossless == 1

        if not os.path.exists(header.bin_dir):
            os.makedirs(header.bin_dir)
        if not os.path.exists(header.log_dir):
            os.makedirs(header.log_dir)
        if not os.path.exists(header.recon_dir):
            os.makedirs(header.recon_dir)
        code_block_size = header.code_block_size
        split_block_height = 2**(header.log2_picture_height_minus6)
        split_block_width = 2**(header.log2_picture_width_minus6)

        bin_name = os.path.splitext(os.path.basename(header.img_name))[0] + '_' + str(header.model_qp) + '_' + str(header.qp_shift)
        ans_enc = rans.RansEncoder()

        trans_steps = 4

        img_path = header.img_name

        logfile = open(os.path.join(header.log_dir, 'enc_log_{}.txt'.format(bin_name)), 'a')

        assert header.model_qp == 19
        assert header.qp_shift == 0
        init_scale = qp_shifts[header.model_qp][header.qp_shift]
        print(init_scale)
        logfile.write(str(init_scale) + '\n')
        logfile.flush()

        with torch.no_grad():
            start = time.time()

            print(img_path)
            logfile.write(img_path + '\n')
            logfile.flush()

            ori_img = Image.open(img_path)
            ori_img = np.array(ori_img, dtype=np.float32)
            original_img_numpy = ori_img
            ori_img = rgb2yuv_lossless(ori_img).astype(np.float32)
            ori_img = torch.from_numpy(ori_img)

            # img -> [n,c,h,w]
            ori_img = ori_img.unsqueeze(0)
            ori_img = ori_img.permute(0, 3, 1, 2)
            # original_img = img

            # img -> (%16 == 0)
            size = ori_img.size()
            height_all = size[2]
            width_all = size[3]
            # encode height and width, in the range of [0, 2^15=32768]
            header.write_header_to_stream(ans_enc, ori_img)

            # split the img into some blocks if the img is too big
            blocks_h = list(torch.split(ori_img, split_block_height, dim=2))
            blocks = [list(torch.split(block_h, split_block_width, dim=3)) for block_h in blocks_h]
            num_blocks = len(blocks)*len(blocks[0])
            print(f'Split the img into blocks, block size is {split_block_height} * {split_block_width}, num_blocks is {num_blocks}')
            logfile.write('Split the img into blocks, block size is: ' + str(split_block_height) + '*' + str(split_block_width) + 'num_blocks is' + str(num_blocks) + '\n')
            logfile.flush()
            
            blocks_rec = []
            block_idx = 0
            for b_h in blocks:
                blocks_h_rec = []
                for block in b_h:
                    print(f'The {block_idx} block is being coded')
                    img = block
                    size = img.size()
                    height = size[2]
                    width = size[3]
                    pad_h = int(np.ceil(height / 16)) * 16 - height
                    pad_w = int(np.ceil(width / 16)) * 16 - width
                    paddings = (0, pad_w, 0, pad_h)
                    img = F.pad(img, paddings, 'replicate')
                    
                    img = img.permute(1, 0, 2, 3)

                    input_img_v = to_variable(img)
                    LL, HL_list, LH_list, HH_list = self.models_dict['transform'].forward_trans(input_img_v)

                    min_v, max_v = find_min_and_max(LL, HL_list, LH_list, HH_list)
                    # print(min_v)
                    # print(max_v)
                    # for all models, the quantized coefficients are in the range of [-6016, 12032]
                    # 15 bits to encode this range
                    for i in range(3):
                        for j in range(13):
                            tmp = min_v[i, j] + 6016
                            write_binary(ans_enc, tmp, 15)
                            tmp = max_v[i, j] + 6016
                            write_binary(ans_enc, tmp, 15)
                    yuv_low_bound = min_v.min(axis=0)
                    yuv_high_bound = max_v.max(axis=0)
                    shift_min = min_v - yuv_low_bound
                    shift_max = max_v - yuv_low_bound

                    subband_h = [(height + pad_h) // 2, (height + pad_h) // 4, (height + pad_h) // 8, (height + pad_h) // 16]
                    subband_w = [(width + pad_w) // 2, (width + pad_w) // 4, (width + pad_w) // 8, (width + pad_w) // 16]

                    padding_sub_h = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_h]
                    padding_sub_w = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_w]

                    coded_coe_num = 0

                    # compress LL
                    tmp_stride = subband_w[3] + padding_sub_w[3]
                    tmp_hor_num = tmp_stride // code_block_size
                    paddings = (0, padding_sub_w[3], 0, padding_sub_h[3])
                    enc_LL = F.pad(LL, paddings, "constant")
                    enc_LL = img2patch(enc_LL, code_block_size, code_block_size, code_block_size)
                    paddings = (6, 6, 6, 6)
                    enc_LL = F.pad(enc_LL, paddings, "constant")

                    temp_add_tensor = 1
                    for h_i in range(code_block_size):
                        for w_i in range(code_block_size):
                            cur_ct = copy.deepcopy(enc_LL[:, :, h_i:h_i + 13, w_i:w_i + 13])
                            cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                            cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                            prob = self.models_dict['entropy_LL'].inf_cdf_lower(cur_ct, yuv_low_bound[0], yuv_high_bound[0]+1)
                            index = enc_LL[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)
                            

                            if temp_add_tensor == 1:
                                temp_add_tensor = 0
                                Lp_list = []
                                for p in range(3):
                                    Lp_list.append(shift_max[p, 0] + 2 - shift_min[p, 0])
                                Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                factor = FACTOR.to(prob.device)
                                new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                for p in range(3):
                                    add_tensor[p] = add_tensor[p] - shift_min[p, 0]
                                if prob.shape[0] != 3:
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                            prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                            for sample_idx, prob_sample in enumerate(prob):
                                coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                        h_i * tmp_stride + \
                                        ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                        w_i
                                if (coe_id % tmp_stride) < subband_w[3] and (coe_id // tmp_stride) < subband_h[3]:
                                    yuv_flag = sample_idx % 3
                                    # if True:
                                    if shift_min[yuv_flag, 0] < shift_max[yuv_flag, 0]:
                                        data = index[sample_idx] - min_v[yuv_flag, 0]
                                        assert data >= 0
                                        quantized_cdf = prob_sample[shift_min[yuv_flag, 0]:shift_max[yuv_flag, 0] + 2]
                                        ans_enc.encode_cdf(data,quantized_cdf)
                                    coded_coe_num = coded_coe_num + 1
                    print('LL encoded...')


                    # LL = LL * used_scale
                    base_context = LL

                    for i in range(trans_steps):
                        j = trans_steps - 1 - i
                        x = 3 * j + 1
                        tmp_stride = subband_w[j] + padding_sub_w[j]
                        tmp_hor_num = tmp_stride // code_block_size
                        # compress HL
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(HL_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        # context = F.pad(LL, paddings, "reflect")
                        if padding_sub_w[j] >= base_context.shape[3] or padding_sub_h[j] >= base_context.shape[2]:
                            context = F.pad(base_context, paddings, "constant")
                        else:
                            context = F.pad(base_context, paddings, "reflect")

                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)


                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = copy.deepcopy(enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13])
                                cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                                cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_HL'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = enc_oth[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)


                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((
                                                        sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        # if True:
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            data = index[sample_idx] - min_v[yuv_flag, x]
                                            assert data >= 0
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            ans_enc.encode_cdf(data,quantized_cdf)
                                        coded_coe_num = coded_coe_num + 1

                        print('HL' + str(j) + ' encoded...')

                        # HL_list[j] = HL_list[j]*used_scale

                        # compress LH
                        x = 3 * j + 2
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(LH_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        # context = F.pad(torch.cat((LL, HL_list[j]), dim=1), paddings, "reflect")
                        temp_context = torch.cat((base_context, HL_list[j]), dim=1)
                        if padding_sub_w[j] >= temp_context.shape[3] or padding_sub_h[j] >= temp_context.shape[2]:
                            context = F.pad(temp_context, paddings, "constant")
                        else:
                            context = F.pad(temp_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)

                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = copy.deepcopy(enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13])
                                cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                                cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_LH'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = enc_oth[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((
                                                        sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        # if True:
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            data = index[sample_idx] - min_v[yuv_flag, x]
                                            assert data >= 0
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            ans_enc.encode_cdf(data,quantized_cdf)
                                        coded_coe_num = coded_coe_num + 1
                        print('LH' + str(j) + ' encoded...')

                        # compress HH
                        x = 3 * j + 3
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(HH_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        # context = F.pad(torch.cat((LL, HL_list[j], LH_list[j]), dim=1), paddings, "reflect")
                        temp_context = torch.cat((base_context, HL_list[j], LH_list[j]), dim=1)
                        if padding_sub_w[j] >= temp_context.shape[3] or padding_sub_h[j] >= temp_context.shape[2]:
                            context = F.pad(temp_context, paddings, "constant")
                        else:
                            context = F.pad(temp_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)

                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = copy.deepcopy(enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13])
                                cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                                cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_HH'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)
                                index = enc_oth[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((
                                                        sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        # if True:
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            data = index[sample_idx] - min_v[yuv_flag, x]
                                            assert data >= 0
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            ans_enc.encode_cdf(data,quantized_cdf)
                                        coded_coe_num = coded_coe_num + 1

                        print('HH' + str(j) + ' encoded...')

                        base_context = torch.cat((base_context, HL_list[j], LH_list[j], HH_list[j]), 1)
                        base_context = F.interpolate(base_context, scale_factor=2, mode='bicubic', align_corners=False)

                    assert (coded_coe_num == (height + pad_h) * (width + pad_w) * 3)
                    
                    # recon
                    for i in range(trans_steps):
                        j = trans_steps-i-1
                        LL = self.models_dict['transform'].inverse_trans(LL, HL_list[j], LH_list[j], HH_list[j], j)
                    
                    recon = LL
                    recon = recon.permute(1, 0, 2, 3)
                    recon = recon[:, :, 0:height, 0:width]
                    blocks_h_rec.append(recon)
                    print(f'The {block_idx} block finished')
                    block_idx += 1
                blocks_rec.append(blocks_h_rec)
            recon = concat(blocks_rec)
            
            recon = recon[0, :, :, :]
            recon = recon.permute(1, 2, 0)
            recon = recon.cpu().data.numpy()
            recon = yuv2rgb_lossless(recon).astype(np.float32)
            
            mse = np.mean((recon - original_img_numpy) ** 2)
            print(mse)

            recon = np.clip(recon, 0., 255.).astype(np.uint8)
            img = Image.fromarray(recon, 'RGB')
            img.save(os.path.join(header.recon_dir, bin_name + '.png'))

            print('encoding finished!')
            logfile.write('encoding finished!' + '\n')
            end = time.time()
            encode_time = end-start
            print('Encoding-time: ', encode_time)
            logfile.write('Encoding-time: ' + str(encode_time) + '\n')

            # bit_out.close()
            rv = ans_enc.flush()
            print('total bytes:', len(rv))
            with open(header.bin_dir + '/' + bin_name + '.bin', "wb") as f:
                write_uchars(f,[0b01010100])
                f.write(rv)
            print('bit_out closed!')
            logfile.write('bit_out closed!' + '\n')

            filesize = len(rv)*8 / height_all / width_all
            print('BPP: ', filesize)
            logfile.write('BPP: ' + str(filesize) + '\n')
            logfile.flush()

            logfile.close()


    def enc_lossy(self, header):

        assert header.coding_tool.isLossless == 0

        if not os.path.exists(header.coding_tool.bin_dir):
            os.makedirs(header.coding_tool.bin_dir)
        if not os.path.exists(header.coding_tool.log_dir):
            os.makedirs(header.coding_tool.log_dir)
        if not os.path.exists(header.coding_tool.recon_dir):
            os.makedirs(header.coding_tool.recon_dir)
        
        code_block_size = header.coding_tool.code_block_size
        split_block_height = 2**(header.log2_slice_size_h_minus6 + 6)
        split_block_width = 2**(header.log2_slice_size_h_minus6 + 6)

        bin_name = os.path.splitext(os.path.basename(header.img_name))[0] + '_' + str(header.parameter_set_id) + '_' + str(header.coding_tool.qp_shift)

        f = open(os.path.join(header.coding_tool.recon_dir, bin_name + '.bin'), 'wb')
        f = header.write_header_to_stream(f)
        f.close()

        ans_enc = rans.RansEncoder()

        trans_steps = 4

        img_path = header.img_name

        logfile = open(os.path.join(header.coding_tool.log_dir, 'enc_log_{}.txt'.format(bin_name)), 'a')

        init_scale = qp_shifts[header.parameter_set_id][header.coding_tool.qp_shift]
        print(init_scale)
        logfile.write(str(init_scale) + '\n')
        logfile.flush()

        with torch.no_grad():
            start = time.time()

            print(img_path)
            logfile.write(img_path + '\n')
            logfile.flush()

            ori_img = Image.open(img_path)
            ori_img = np.array(ori_img, dtype=np.float32)
            original_img_numpy = ori_img
            ori_img = torch.from_numpy(ori_img)
            # img -> [n,c,h,w]
            ori_img = ori_img.unsqueeze(0)
            ori_img= ori_img.permute(0, 3, 1, 2)

            # img -> (%16 == 0)
            size = ori_img.size()
            height_all = size[2]
            width_all = size[3]
            # encode height and width, in the range of [0, 2^15=32768]
            # header.write_header_to_stream(enc)
            
            # split the img into some blocks if the img is too big
            blocks_h = list(torch.split(ori_img, split_block_height, dim=2))
            blocks = [list(torch.split(block_h, split_block_width, dim=3)) for block_h in blocks_h]
            num_blocks = len(blocks)*len(blocks[0])
            print(f'Split the img into blocks, block size is {split_block_height} * {split_block_width}, num_blocks is {num_blocks}')
            logfile.write('Split the img into blocks, block size is: ' + str(split_block_height) + '*' + str(split_block_width) + 'num_blocks is' + str(num_blocks) + '\n')
            logfile.flush()
            
            blocks_rec = []
            block_idx = 0
            for b_h in blocks:
                blocks_h_rec = []
                for block in b_h:
                    print(f'The {block_idx} block is being coded')
                    img = block
                    size = img.size()
                    height = size[2]
                    width = size[3]
                    pad_h = int(np.ceil(height / 16)) * 16 - height
                    pad_w = int(np.ceil(width / 16)) * 16 - width
                    paddings = (0, pad_w, 0, pad_h)
                    img = F.pad(img, paddings, 'replicate')
                    
                    # img -> [3,1,h,w], YUV in batch dim
                    img = rgb2yuv(img)
                    img = img.permute(1, 0, 2, 3)

                    input_img_v = to_variable(img)
                    LL, HL_list, LH_list, HH_list, used_scale = self.models_dict['transform'].forward_trans(input_img_v)

                    min_v, max_v = find_min_and_max(LL, HL_list, LH_list, HH_list)

                    # for all models, the quantized coefficients are in the range of [-6016, 12032]
                    # 15 bits to encode this range
                    for i in range(3):
                        for j in range(13):
                            tmp = min_v[i, j] + 6016
                            write_binary(ans_enc, tmp, 15)
                            tmp = max_v[i, j] + 6016
                            write_binary(ans_enc, tmp, 15)
                    yuv_low_bound = min_v.min(axis=0)
                    yuv_high_bound = max_v.max(axis=0)
                    shift_min = min_v - yuv_low_bound
                    shift_max = max_v - yuv_low_bound

                    subband_h = [(height + pad_h) // 2, (height + pad_h) // 4, (height + pad_h) // 8, (height + pad_h) // 16]
                    subband_w = [(width + pad_w) // 2, (width + pad_w) // 4, (width + pad_w) // 8, (width + pad_w) // 16]

                    padding_sub_h = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_h]
                    padding_sub_w = [(int(np.ceil(tmp / code_block_size)) * code_block_size - tmp) for tmp in subband_w]

                    coded_coe_num = 0

                    # compress LL
                    tmp_stride = subband_w[3] + padding_sub_w[3]
                    tmp_hor_num = tmp_stride // code_block_size
                    paddings = (0, padding_sub_w[3], 0, padding_sub_h[3])
                    enc_LL = F.pad(LL, paddings, "constant")
                    enc_LL = img2patch(enc_LL, code_block_size, code_block_size, code_block_size)
                    paddings = (6, 6, 6, 6)
                    enc_LL = F.pad(enc_LL, paddings, "constant")

                    temp_add_tensor = 1
                    for h_i in range(code_block_size):
                        for w_i in range(code_block_size):
                            
                            cur_ct = copy.deepcopy(enc_LL[:, :, h_i:h_i + 13, w_i:w_i + 13])
                            cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                            cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                            prob = self.models_dict['entropy_LL'].inf_cdf_lower(cur_ct, yuv_low_bound[0], yuv_high_bound[0]+1)
                            index = enc_LL[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)
                            

                            if temp_add_tensor == 1:
                                temp_add_tensor = 0
                                Lp_list = []
                                for p in range(3):
                                    Lp_list.append(shift_max[p, 0] + 2 - shift_min[p, 0])
                                Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                factor = FACTOR.to(prob.device)
                                new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                for p in range(3):
                                    add_tensor[p] = add_tensor[p] - shift_min[p, 0]
                                if prob.shape[0] != 3:
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                            prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                            for sample_idx, prob_sample in enumerate(prob):
                                coe_id = ((sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                        h_i * tmp_stride + \
                                        ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                        w_i
                                if (coe_id % tmp_stride) < subband_w[3] and (coe_id // tmp_stride) < subband_h[3]:
                                    yuv_flag = sample_idx % 3
                                    # if True:
                                    if shift_min[yuv_flag, 0] < shift_max[yuv_flag, 0]:
                                        data = index[sample_idx] - min_v[yuv_flag, 0]
                                        assert data >= 0
                                        quantized_cdf = prob_sample[shift_min[yuv_flag, 0]:shift_max[yuv_flag, 0] + 2]
                                        ans_enc.encode_cdf(data,quantized_cdf)
                                    coded_coe_num = coded_coe_num + 1
                    print('LL encoded...')


                    LL = LL * used_scale
                    base_context = LL

                    for i in range(trans_steps):
                        j = trans_steps - 1 - i
                        x = 3 * j + 1
                        tmp_stride = subband_w[j] + padding_sub_w[j]
                        tmp_hor_num = tmp_stride // code_block_size
                        # compress HL
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(HL_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        # context = F.pad(LL, paddings, "reflect")
                        if padding_sub_w[j] >= base_context.shape[3] or padding_sub_h[j] >= base_context.shape[2]:
                            context = F.pad(base_context, paddings, "constant")
                        else:
                            context = F.pad(base_context, paddings, "reflect")

                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)

                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = copy.deepcopy(enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13])
                                cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                                cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_HL'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = enc_oth[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)


                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((
                                                        sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        # if True:
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            data = index[sample_idx] - min_v[yuv_flag, x]
                                            assert data >= 0
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            ans_enc.encode_cdf(data,quantized_cdf)
                                        coded_coe_num = coded_coe_num + 1
                        print('HL' + str(j) + ' encoded...')


                        HL_list[j] = HL_list[j] * used_scale

                        # compress LH
                        x = 3 * j + 2
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(LH_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        # context = F.pad(torch.cat((LL, HL_list[j]), dim=1), paddings, "reflect")
                        temp_context = torch.cat((base_context, HL_list[j]), dim=1)
                        if padding_sub_w[j] >= temp_context.shape[3] or padding_sub_h[j] >= temp_context.shape[2]:
                            context = F.pad(temp_context, paddings, "constant")
                        else:
                            context = F.pad(temp_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)

                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = copy.deepcopy(enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13])
                                cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                                cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_LH'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)

                                index = enc_oth[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((
                                                        sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        # if True:
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            data = index[sample_idx] - min_v[yuv_flag, x]
                                            assert data >= 0
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            ans_enc.encode_cdf(data,quantized_cdf)
                                        coded_coe_num = coded_coe_num + 1
                        print('LH' + str(j) + ' encoded...')


                        LH_list[j] = LH_list[j] * used_scale

                        # compress HH
                        x = 3 * j + 3
                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        enc_oth = F.pad(HH_list[j], paddings, "constant")
                        enc_oth = img2patch(enc_oth, code_block_size, code_block_size, code_block_size)
                        paddings = (6, 6, 6, 6)
                        enc_oth = F.pad(enc_oth, paddings, "constant")

                        paddings = (0, padding_sub_w[j], 0, padding_sub_h[j])
                        # context = F.pad(torch.cat((LL, HL_list[j], LH_list[j]), dim=1), paddings, "reflect")
                        temp_context = torch.cat((base_context, HL_list[j], LH_list[j]), dim=1)
                        if padding_sub_w[j] >= temp_context.shape[3] or padding_sub_h[j] >= temp_context.shape[2]:
                            context = F.pad(temp_context, paddings, "constant")
                        else:
                            context = F.pad(temp_context, paddings, "reflect")
                        paddings = (6, 6, 6, 6)
                        context = F.pad(context, paddings, "reflect")
                        context = img2patch_padding(context, code_block_size + 12, code_block_size + 12, code_block_size, 6)
                        
                        temp_add_tensor = 1
                        for h_i in range(code_block_size):
                            for w_i in range(code_block_size):
                                cur_ct = copy.deepcopy(enc_oth[:, :, h_i:h_i + 13, w_i:w_i + 13])
                                cur_ct[:, :, 13 // 2 + 1:13, :] = 0.
                                cur_ct[:, :, 13 // 2, 13 // 2:13] = 0.
                                cur_context = context[:, :, h_i:h_i + 13, w_i:w_i + 13]
                                prob = self.models_dict['entropy_HH'].inf_cdf_lower(cur_ct, cur_context,
                                                                yuv_low_bound[x], yuv_high_bound[x]+1, j)
                                index = enc_oth[:, 0, h_i + 6, w_i + 6].cpu().data.numpy().astype(np.int)

                                if temp_add_tensor == 1:
                                    temp_add_tensor = 0
                                    Lp_list = []
                                    for p in range(3):
                                        Lp_list.append(shift_max[p, x] + 2 - shift_min[p, x])
                                    Lp_tensor = torch.tensor(Lp_list * (prob.shape[0]//3))
                                    factor = FACTOR.to(prob.device)
                                    new_max_value = (factor + 1 - Lp_tensor.to(prob.device)).unsqueeze(1)
                                    add_tensor = torch.arange(prob.shape[-1], dtype=torch.int32, device=prob.device).repeat(3,1)
                                    for p in range(3):
                                        add_tensor[p] = add_tensor[p] - shift_min[p, x]
                                    add_tensor = add_tensor.repeat(prob.shape[0]//3,1)
                            
                                prob = prob.mul(new_max_value).add(add_tensor).round().to(dtype=torch.int32, non_blocking=True).tolist()

                                for sample_idx, prob_sample in enumerate(prob):
                                    coe_id = ((
                                                        sample_idx // 3) // tmp_hor_num) * tmp_hor_num * code_block_size * code_block_size + \
                                            h_i * tmp_stride + \
                                            ((sample_idx // 3) % tmp_hor_num) * code_block_size + \
                                            w_i
                                    if (coe_id % tmp_stride) < subband_w[j] and (coe_id // tmp_stride) < subband_h[j]:
                                        yuv_flag = sample_idx % 3
                                        # if True:
                                        if shift_min[yuv_flag, x] < shift_max[yuv_flag, x]:
                                            data = index[sample_idx] - min_v[yuv_flag, x]
                                            assert data >= 0
                                            quantized_cdf = prob_sample[shift_min[yuv_flag, x]:shift_max[yuv_flag, x] + 2]
                                            ans_enc.encode_cdf(data,quantized_cdf)
                                        coded_coe_num = coded_coe_num + 1
                        print('HH' + str(j) + ' encoded...')

                        HH_list[j] = HH_list[j] * used_scale

                        base_context = torch.cat((base_context, HL_list[j], LH_list[j], HH_list[j]), 1)
                        base_context = F.interpolate(base_context, scale_factor=2, mode='bicubic', align_corners=False)

                    assert (coded_coe_num == (height + pad_h) * (width + pad_w) * 3)

                    # recon
                    for i in range(trans_steps):
                        j = trans_steps-i-1
                        LL = self.models_dict['transform'].inverse_trans(LL, HL_list[j], LH_list[j], HH_list[j], j)
                    
                    recon = LL
                    recon = yuv2rgb(recon.permute(1, 0, 2, 3))
                    recon = recon[:, :, 0:height, 0:width]
                    blocks_h_rec.append(recon)
                    print(f'The {block_idx} block finished')
                    block_idx += 1
                blocks_rec.append(blocks_h_rec)
            recon = concat(blocks_rec)

            if header.coding_tool.filtering_model_id == 1:

                recon = self.models_dict['post'](recon)

                if height_all * width_all > 1080 * 1920:
                    h_list = [0, height_all // 2, height_all]
                    w_list = [0, width_all // 2, width_all]
                    k_ = 2
                else:
                    h_list = [0, height_all]
                    w_list = [0, width_all]
                    k_ = 1
                gan_rgb_post = torch.zeros_like(recon)
                for _i in range(k_):
                    for _j in range(k_):
                        pad_start_h = max(h_list[_i] - 64, 0) - h_list[_i]
                        pad_end_h = min(h_list[_i + 1] + 64, height_all) - h_list[_i + 1]
                        pad_start_w = max(w_list[_j] - 64, 0) - w_list[_j]
                        pad_end_w = min(w_list[_j + 1] + 64, width_all) - w_list[_j + 1]
                        tmp = self.models_dict['postGAN'](recon[:, :, h_list[_i] + pad_start_h:h_list[_i + 1] + pad_end_h,
                                                     w_list[_j] + pad_start_w:w_list[_j + 1] + pad_end_w])
                        gan_rgb_post[:, :, h_list[_i]:h_list[_i + 1], w_list[_j]:w_list[_j + 1]] = tmp[:, :,
                                                                                                   -pad_start_h:
                                                                                                   tmp.size()[
                                                                                                       2] - pad_end_h,
                                                                                                   -pad_start_w:
                                                                                                   tmp.size()[
                                                                                                       3] - pad_end_w]
                recon = gan_rgb_post
            else:

                h_list = [0, height_all // 3, height_all // 3 * 2, height_all]
                w_list = [0, width_all // 3, width_all // 3 * 2, width_all]
                k_ = 3
                rgb_post = torch.zeros_like(recon)
                for _i in range(k_):
                    for _j in range(k_):
                        pad_start_h = max(h_list[_i] - 64, 0) - h_list[_i]
                        pad_end_h = min(h_list[_i + 1] + 64, height_all) - h_list[_i + 1]
                        pad_start_w = max(w_list[_j] - 64, 0) - w_list[_j]
                        pad_end_w = min(w_list[_j + 1] + 64, width_all) - w_list[_j + 1]
                        tmp = self.models_dict['post'](recon[:, :, h_list[_i] + pad_start_h:h_list[_i + 1] + pad_end_h,
                                                  w_list[_j] + pad_start_w:w_list[_j + 1] + pad_end_w])
                        rgb_post[:, :, h_list[_i]:h_list[_i + 1], w_list[_j]:w_list[_j + 1]] = tmp[:, :,
                                                                                               -pad_start_h:tmp.size()[
                                                                                                                2] - pad_end_h,
                                                                                               -pad_start_w:tmp.size()[
                                                                                                                3] - pad_end_w]
                recon = rgb_post

            recon = torch.clamp(torch.round(recon), 0., 255.)

            recon = recon[0, :, :, :]
            recon = recon.permute(1, 2, 0)
            recon = recon.cpu().data.numpy().astype(np.uint8)
            rgb_psnr, rgb_scaled_msssim, yuv_psnr, y_scaled_msssim = evaluate(recon, original_img_numpy)
            img = Image.fromarray(recon, 'RGB')
            img.save(os.path.join(header.recon_dir, bin_name + '.png'))

            print('encoding finished!')
            logfile.write('encoding finished!' + '\n')
            end = time.time()
            encode_time = end-start
            print('Encoding-time: ', encode_time)
            logfile.write('Encoding-time: ' + str(encode_time) + '\n')

            # bit_out.close()
            rv = ans_enc.flush()
            print('total bytes:', len(rv))
            with open(header.bin_dir + '/' + bin_name + '.bin', "wb") as f:
                write_uchars(f,[0b01010100])
                f.write(rv)
            print('bit_out closed!')
            logfile.write('bit_out closed!' + '\n')

            filesize = len(rv)*8 / height_all / width_all
            print('BPP: ', filesize)
            logfile.write('BPP: ' + str(filesize) + '\n')
            logfile.flush()

            print('RGB_PSNR: ', rgb_psnr)
            logfile.write('RGB_PSNR: ' + str(rgb_psnr) + '\n')
            logfile.flush()
        
            print('RGB_SSIM: ', rgb_scaled_msssim)
            logfile.write('RGB_SSIM: ' + str(rgb_scaled_msssim) + '\n')
            logfile.flush()
            
            print('YUV_PSNR: ', yuv_psnr)
            logfile.write('YUV_PSNR: ' + str(yuv_psnr) + '\n')
            logfile.flush()
            
            print('Y_SSIM: ', y_scaled_msssim)
            logfile.write('Y_SSIM: ' + str(y_scaled_msssim) + '\n')
            logfile.flush()
            logfile.close()
        
        return filesize, rgb_psnr, rgb_scaled_msssim, yuv_psnr, y_scaled_msssim, encode_time
