import os, time
import struct
import numpy as np
import torch, rans
import torch.nn as nn
from Common.utils.iWave_utils.metric.metrics import evaluate
from Common.utils.syntaxparse.parse import picture
from Common.utils.nic_utils.config import dict
from Common.models.NIC_models.acceleration_utils import unpack_bools
from Common.HighLevel_Function.Bin_Symbol import NIC_Bin2Symbol,BEE_Bin2Symbol,iWave_Bin2Symbol
from Common.HighLevel_Function.Bits_Bin import NIC_Bits2Bin,BEE_Bits2Bin,iWave_Bits2Bin
from Common.HighLevel_Function.InverseTrans import NIC_InverseTrans,BEE_InverseTrans,iWave_InverseTrans
from Common.HighLevel_Function.Preprocess import NIC_Preprocess,BEE_Preprocess,iWave_Preprocess
from Common.HighLevel_Function.Symbol_Bin import NIC_Symbol2Bin,BEE_Symbol2Bin
from Common.HighLevel_Function.Bin_Bits import NIC_Bin2Bits,BEE_Bin2Bits
from Common.HighLevel_Function.Postprocess import NIC_Postprocess,BEE_Postprocess,iWave_Postprocess
from Common.HighLevel_Function.ForwardTrans import NIC_ForwardTrans,BEE_ForwardTrans,iWave_ForwardTrans
from PIL import Image


class ModelEngine(nn.Module):
    def __init__(self):
        super().__init__()
        self.picture = picture()
        # Encoder Components
        self.preprocess = None
        self.trans = None
        self.symbol2bin = None
        self.bin2bits = None
        # Decoder Components
        self.bits2bin = None
        self.bin2symbol = None
        self.inverse_trans = None
        self.postprocess = None

    def parse_syntax(self, string):
        self.picture.read_header_from_stream(string)

    def read_cfg(self, cfgfile, image, rate):
        self.picture.read_json_cfg(cfgfile, image, rate)
        assert self.picture.picture_header.profile_id != None

    def build(self, codectype):
        if codectype == "Enc":
            self.initialpreprocess()
            self.initialtrans()
            self.initialsymbol2bin()
            self.initialbin2bits()
            return
        elif codectype == "Dec":
            self.initialbits2bin()
            self.initialbin2symbol()
            self.initialinversetrans()
            self.initialpostprocess()
            return

    def to(self, device):
        for m in self.children():
            m.to(device)

    def encode(self, cfg, image, rate, ckptdir, binName, device):
        self.read_cfg(cfg, image, rate)
        self.build("Enc")
        if self.picture.picture_header.profile_id == 0:
            if self.picture.picture_header.model_id == 0:
                model_path = os.path.join(ckptdir,
                                          f"quant_model.ckpt-{self.picture.picture_header.parameter_set_id + 1:02d}")
                self.modelload("Enc", model_path, device)
                self.to(torch.device(device))
                self.eval()
                with torch.no_grad():
                    input_variable = self.preprocess.encode(image, self.picture, device)
                    self.picture = input_variable[-1]
                    forward_trans_varlist = self.forward_trans.encode(input_variable[:2])
                    symbol2bin_varlist = self.symbol2bin.encode(forward_trans_varlist, self.picture, device)
                    self.bin2bits.encode(symbol2bin_varlist, self.picture, binName)
            else:
                raise ValueError(f"model_id can only be 0, received {self.picture.picture_header.model_id}")
        
        if self.picture.picture_header.profile_id == 1:
            self.encode_block(image, ckptdir, binName, device)

        if self.picture.picture_header.profile_id == 2:
            model_path = self.picture.picture_header.coding_tool.model_path
            self.modelload("Enc", model_path, device)
            self.forward_trans.encode(image, self.picture.picture_header)

    def encode_block(self, image, model_list, binName, device):
        img_class_path = os.path.dirname(os.path.dirname(binName))  ## bin上上级目录（默认bin路径：classC_2k/lambda_0018/Enc/a.bin）  ## 编码时保存的重见图片路径
        img_name = binName.split('/')[-1].split(".")[0]
        Rec_dir = os.path.join(img_class_path, f'{img_name}.png')
        print("img_class:", Rec_dir)

        picture = self.picture
        slice = self.picture.slice
        picture_header = self.picture.picture_header
        # slice_header = self.header.picture.slice_header
        picture_header.GPU = dict["GPU"]
        
        self.modelload("Enc", model_list, device)
        
        self.to(device)
        
        self.eval()  ## ad
        start = time.time()
        slice.slice_header.profile_id = picture_header.profile_id
        source_img, img_slice_list = self.preprocess.encode(image, picture, device)

        f = open(binName, 'wb')
        f = picture.picture_header.write_header_to_stream(f)

        # if use filtering, need to reconstruct image at encoder side
        # TODO: what is out_img for?
        # if picture_header.coding_tool.filtering_flag:
            # out_img = np.zeros([picture_header.picture_size_h, picture_header.picture_size_w, picture.C]) # recon image
            # slice.H_offset = 0
            # slice.W_offset = 0
        out_img = np.zeros([picture_header.picture_size_h, picture_header.picture_size_w, picture_header.C]) # recon image
        slice.H_offset = 0
        slice.W_offset = 0
            
        ######################### Padding Image #########################
        tile = 64.
        Slice_Idx = 0
        print("[Encoding Slice] Slice Num:", len(img_slice_list))
        picture.slice.slice_header.geo_flag = picture_header.coding_tool.geo_flag
        
        for img in img_slice_list:  # Traverse CTUs
            self.picture.slice.slice_header.slice_id = Slice_Idx
            slice_H, slice_W = img.shape[0], img.shape[1]
            print("Encoding Slice:", Slice_Idx, slice_H, slice_W)
            slice_H_PAD = int(tile * np.ceil(slice_H / tile))
            slice_W_PAD = int(tile * np.ceil(slice_W / tile))
            im = np.zeros([slice_H_PAD, slice_W_PAD, 3], dtype='float32')
            
            # TODO: channel = 3?
            im[:slice.slice_header.height, :slice.slice_header.width, :] = img[:, :, :3]
            
            im = torch.FloatTensor(im)
            im = im.permute(2, 0, 1).contiguous()
            im = im.view(1, picture_header.C, slice_H_PAD, slice_W_PAD)
            # if picture.GPU:
                # im = im.cuda()
                # if picture.model_id == 0 or picture.model_id == 1:
                    # picture.lambda_rd = picture.lambda_rd.cuda()
            im = im.to(device)
            if picture_header.model_id == 0 or picture_header.model_id == 1:
                picture_header.lambda_rd = picture_header.lambda_rd.to(device)            
            # print('====> Encoding Image:', im_dir, "%dx%d" % (block_H, block_W), 'to', out_dir,
            #     " Block Idx: %d" % (Block_Idx))
            Slice_Idx += 1
            # begin processing CTU
            im_slice_list = []
            im_slice_loc_list = []
            im_slice_list.append(im)  # list size = 1
            im_slice_loc_list.append([0, 0, slice_H_PAD, slice_W_PAD])
            for im_slice_loc, im_slice in zip(im_slice_loc_list, im_slice_list):
                im_slice = self.preprocess.process_slice(im_slice, picture)
            forward_trans_varlist = self.forward_trans.encode(im_slice, picture_header)
            symbol2bin_varlist = self.symbol2bin.encode(forward_trans_varlist, out_img, picture, device)
            self.bin2bits.encode(symbol2bin_varlist, picture, f, Slice_Idx)
        self.postprocess.encode(source_img, out_img, f, picture, Rec_dir)  ## 编码时也重建图片，路径Rec_dir

        # added for test, can be deleted
        ###########################################
        print('encoding finished!')
        end = time.time()
        encode_time = end-start
        print('Encoding-time: ', encode_time)
        filesize = os.path.getsize(binName) * 8
        BPP = filesize / H / W
        print('BPP: ', BPP)
        if header.USE_POSTPROCESSING:
            rgb_psnr, rgb_scaled_msssim, yuv_psnr, y_scaled_msssim = evaluate(out_img, source_img)
            print('RGB_PSNR: ', rgb_psnr)
            print('RGB_SSIM: ', rgb_scaled_msssim)
            print('YUV_PSNR: ', yuv_psnr)
            print('Y_SSIM: ', y_scaled_msssim)
        ###########################################
            

    def decode(self, string, recon_path, ckptdir, device):
        self.parse_syntax(string)
        self.build("Dec")
        if self.picture.picture_header.profile_id == 0:
            model_path = os.path.join(ckptdir,
                                      f"quant_model.ckpt-{self.picture.picture_header.parameter_set_id + 1:02d}")
            self.modelload("Dec", model_path, device)
            self.to(torch.device(device))
            self.eval()
            bits2bin_varlist = self.bits2bin.decode(self.picture, device)
            quant_latent = self.bin2symbol.decode(bits2bin_varlist, device)
            imArray = self.inverse_trans.decode(quant_latent)
            self.postprocess.decode(imArray, self.picture, recon_path)
        
        if self.picture.picture_header.profile_id == 1:
            # print("FILTERING FLAG:", self.picture.picture_header.coding_tool.filtering_flag)
            self.decode_block(string, recon_path, ckptdir, device)
            
        if self.picture.picture_header.profile_id == 2:
            self.modelload("Dec", ckptdir, device)
            img_array = self.inverse_trans.decode(self.picture, recon_path)
            if self.picture.picture_header.isLossless == 0:
                self.postprocess.decode(img_array, self.picture, recon_path)
        return

    def decode_block(self, f, recon_path, ckptdir, device):
        
        self.modelload("Dec", ckptdir, device)
        # self.to(device)
        self.eval()
        header = self.picture.picture_header
        slice_header = self.picture.slice.slice_header

        Block_Idx = -1
        imArray = np.zeros([header.picture_size_h, header.picture_size_w, 3])
        H_offset, W_offset = 0, 0
        print("Slice Num:", header.Slice_Num_in_Height, header.Slice_Num_in_Width)
        for i_block in range(header.Slice_Num_in_Height):
            for j_block in range(header.Slice_Num_in_Width):
                print(i_block, j_block)
                Block_Idx += 1
                header.USE_ACCELERATION = 0
                if header.USE_ACCELERATION:
                    if Block_Idx == 0:
                        EC_this_block = unpack_bools(self.bits2bin.c_main, self.header.picture.f)
                    else:
                        b_len = struct.calcsize('1?')
                        bits = self.header.picture.f.read(b_len)
                        b = struct.unpack('1?', bits)
                        if b == 1:
                            EC_this_block = EC_last_block
                        else:
                            EC_this_block = unpack_bools(self.bits2bin.c_main, self.header.picture.f)
                    EC_last_block = EC_this_block
                block_H = header.slice_height
                block_W = header.slice_width

                if header.USE_ACCELERATION:
                    slice_header.EC_this_block = EC_this_block
                else:
                    slice_header.EC_this_block = [1] * self.bits2bin.c_main
                if i_block == header.Slice_Num_in_Height - 1:
                    block_H = header.picture_size_h - (header.Slice_Num_in_Height - 1) * block_H
                if j_block == header.Slice_Num_in_Width - 1:
                    block_W = header.picture_size_w - (header.Slice_Num_in_Width - 1) * block_W
                print('==================> Decoding Block:', "(%d, %d)" % (i_block, j_block),
                      "[%d, %d]" % (block_H, block_W))
                tile = 64.
                block_H_PAD = int(tile * np.ceil(block_H / tile))
                block_W_PAD = int(tile * np.ceil(block_W / tile))
                block_loc_list = []
                block_loc_list.append([0, 0, block_H_PAD, block_W_PAD])
                for slice_loc in block_loc_list:
                    # slice_loc -> [vertical_location, horizontal_location, block_height, block_width]
                    print('==================> Decoding sub_block:',
                          "(%d, %d, %d, %d)" % (slice_loc[0], slice_loc[1], slice_loc[2], slice_loc[3]))
                    slice_header.slice_loc = slice_loc
                    slice_header.geo_flag = header.coding_tool.geo_flag
                    slice_header.read_header_from_stream(f, header.profile_id)

                    if slice_header.geo_flag:
                        if slice_header.geo_index % 2 == 0:
                            # [enc_height, enc_width] indicates shape of encoding block (after geometrical operation)
                            slice_header.enc_height = slice_loc[2]
                            slice_header.enc_width = slice_loc[3]
                        else:
                            slice_header.enc_height = slice_loc[3]
                            slice_header.enc_width = slice_loc[2]
                    else:
                        slice_header.enc_height = slice_loc[2]
                        slice_header.enc_width = slice_loc[3]

                    y_hyper_q, slice_header, bin_dir_path = self.bits2bin.decode(f, header, slice_header)
                    # quant_latent = self.bin2symbol.decode(y_hyper_q, header, slice_header)
                    ## 改，传入输入的bin文件路径。self.header.picture.f为bin文件名
                    quant_latent = self.bin2symbol.decode(y_hyper_q, header, slice_header, bin_dir_path, dec)
                    imArray_block = self.inverse_trans.decode(quant_latent, header, slice_header)
                    imArray[H_offset: H_offset + block_H, W_offset: W_offset + block_W, :] = imArray_block[:block_H, :block_W, :]
                del block_loc_list
                W_offset += block_W
                if W_offset >= header.picture_size_w:
                    W_offset = 0
                    H_offset += block_H
        self.postprocess.decode(imArray, f, header, recon_path)
        return

    def modelload(self, codectype, ckpt, device):
        if self.picture.picture_header.profile_id == 0:
            checkpoint = torch.load(ckpt, map_location=torch.device(device))
            if codectype == "Enc":
                self.bin2bits.load_state_dict(checkpoint, strict=False)
                self.symbol2bin.load_state_dict(checkpoint, strict=False)
                self.forward_trans.load_state_dict(checkpoint, strict=False)
            if codectype == "Dec":
                self.bits2bin.load_state_dict(checkpoint, strict=False)
                self.bin2symbol.load_state_dict(checkpoint, strict=False)
                self.inverse_trans.load_state_dict(checkpoint, strict=False)
    
        if self.picture.picture_header.profile_id == 1:
            # USE_VR_MODEL = dict['USE_VR_MODEL']
            if self.picture.picture_header.model_id == 0:
                models = ["mse_VR_low", "msssim_VR_low"]
                # self.picture.picture_header.max_lambdas = [6, 0.08]
            if self.picture.picture_header.model_id == 1:
                models = ["mse_VR_middle", "mse_VR_high", "msssim_VR_middle", "msssim_VR_high"]
                # self.picture.picture_header.max_lambdas = [44, 296, 0.96, 7.68]
            if self.picture.picture_header.model_id == 2:
                models = ["mse10", "mse42", "mse160", "mse618", "mse2950",
                          "msssim140", "msssim650", "msssim2700", "msssim11537", "msssim44000"]
            if self.picture.picture_header.model_id > 2:
                raise NotImplementedError
            
            model_index = models[self.picture.picture_header.parameter_set_id]
            checkpoint = torch.load(os.path.join(ckpt, model_index + r'.pkl'), map_location='cpu')
            checkpoint_context = torch.load(os.path.join(ckpt, model_index + r'p.pkl'), map_location='cpu')
            if codectype == "Dec":
                # self.bits2bin.load_state_dict(torch.load(
                #     os.path.join(ckpt, model_index + r'.pkl'), map_location='cpu'), strict=False)
                # self.bin2symbol.load_state_dict(torch.load(
                #     os.path.join(ckpt, model_index + r'.pkl'), map_location='cpu'), strict=False)
                # self.inverse_trans.load_state_dict(torch.load(
                #     os.path.join(ckpt, model_index + r'.pkl'), map_location='cpu'), strict=False)
                # self.bin2symbol.context.load_state_dict(torch.load(
                    # os.path.join(ckpt, model_index + r'p.pkl'), map_location='cpu'))
                self.bits2bin.load_state_dict(checkpoint, strict=False)
                self.bin2symbol.load_state_dict(checkpoint, strict=False)
                self.inverse_trans.load_state_dict(checkpoint, strict=False)
                self.bin2symbol.context.load_state_dict(checkpoint_context)
                if dict['GPU']: # to be removed
                    self.bits2bin = self.bits2bin.cuda()
                    self.bin2symbol = self.bin2symbol.cuda()
                    self.inverse_trans = self.inverse_trans.cuda()
                    # self.bin2symbol.context = self.bin2symbol.context.cuda()
            if codectype == "Enc":
                ## added
                # self.preprocess = model_load(self.preprocess, checkpoint)
                self.preprocess.image_comp.load_state_dict(checkpoint, strict=False)  ## 改
                
                # ae and context models need to be loaded before preprocess 
                self.preprocess.context.load_state_dict(checkpoint_context)
                
                self.bin2bits.load_state_dict(checkpoint, strict=False)
                self.symbol2bin.load_state_dict(checkpoint, strict=False)
                # self.forward_trans.load_state_dict(checkpoint, strict=False)
                self.symbol2bin.context.load_state_dict(checkpoint_context)
                
        if self.picture.picture_header.profile_id == 2:
            pic_header = self.picture.picture_header.coding_tool
            if pic_header.isLossless:
                qp_shift = 0
                model_qp = 19
                init_scale = pic_header.qp_shifts[model_qp][qp_shift]
                checkpoint = torch.load(ckpt + '/' + str(pic_header.model_lambdas[model_qp]) + '_lossless.pth', map_location=torch.device('cpu'))
                all_part_dict = checkpoint['state_dict']

                models_dict = {}
                models_dict['transform'] = self.bits2bin.Transform_lossless
                models_dict['entropy_LL'] = self.bits2bin.CodingLL_lossless
                models_dict['entropy_HL'] = self.bits2bin.CodingHL_lossless
                models_dict['entropy_LH'] = self.bits2bin.CodingLH_lossless
                models_dict['entropy_HH'] = self.bits2bin.CodingHH_lossless
                models_dict_update = {}
                for key, model in models_dict.items():
                    myparams_dict = model.state_dict()
                    part_dict = {k: v for k, v in all_part_dict.items() if k in myparams_dict}
                    myparams_dict.update(part_dict)
                    model.load_state_dict(myparams_dict)
                    if torch.cuda.is_available():
                        model = model.cuda()
                        model.eval()
                    models_dict_update[key] = model
                models_dict.update(models_dict_update)

                if codectype == "Dec":
                    self.inverse_trans.models_dict = models_dict
                elif codectype == "Enc":
                    self.forward_trans.models_dict = models_dict
            else:
                model_qp = self.picture.picture_header.parameter_set_id

                if model_qp > 4.5:  # 5-->26 Perceptual models
                    checkpoint = torch.load(ckpt + '/' + str(pic_header.model_lambdas[model_qp]) + '_percep.pth')
                else:  # 0-->4 MSE models
                    checkpoint = torch.load(ckpt + '/' + str(pic_header.model_lambdas[model_qp]) + '_mse.pth')

                all_part_dict = checkpoint['state_dict']

                models_dict = {}

                models_dict['transform'] = self.bits2bin.Transform_aiWave
                models_dict['entropy_LL'] = self.bits2bin.CodingLL
                models_dict['entropy_HL'] = self.bits2bin.CodingHL
                models_dict['entropy_LH'] = self.bits2bin.CodingLH
                models_dict['entropy_HH'] = self.bits2bin.CodingHH
                models_dict['post'] = self.bits2bin.Post
                if pic_header.filtering_model_id == 1:
                    models_dict['postGAN'] = self.bits2bin.PostGAN

                models_dict_update = {}
                for key, model in models_dict.items():
                    myparams_dict = model.state_dict()
                    part_dict = {k: v for k, v in all_part_dict.items() if k in myparams_dict}
                    myparams_dict.update(part_dict)
                    model.load_state_dict(myparams_dict)
                    if torch.cuda.is_available():
                        model = model.cuda()
                        model.eval()
                    models_dict_update[key] = model
                models_dict.update(models_dict_update)

                if codectype == "Dec":
                    self.inverse_trans.models_dict = models_dict
                    self.postprocess.model_post = models_dict['post']
                    if pic_header.filtering_model_id == 1:
                        self.postprocess.model_postGAN = models_dict['postGAN']
                elif codectype == "Enc":
                    self.forward_trans.models_dict = models_dict
                    self.postprocess.model_post = models_dict['post']
                    if pic_header.filtering_model_id == 1:
                        self.postprocess.model_postGAN = models_dict['postGAN']

    def initialpreprocess(self):
        if self.picture.picture_header.profile_id == 0:
            self.preprocess = BEE_Preprocess()

        if self.picture.picture_header.profile_id == 1:
            self.preprocess = NIC_Preprocess(self.picture.picture_header)
            self.net, self.context = self.preprocess.image_comp, self.preprocess.context

        if self.picture.picture_header.profile_id == 2:
            self.preprocess = iWave_Preprocess()
        return

    def initialtrans(self):
        if self.picture.picture_header.profile_id == 0:
            self.forward_trans = BEE_ForwardTrans()
            self.forward_trans.update(self.picture)
            return
        
        if self.picture.picture_header.profile_id == 1:
            self.forward_trans = NIC_ForwardTrans(self.net.encoder)
            return 
        
        if self.picture.picture_header.profile_id == 2:
            self.bits2bin = iWave_Bits2Bin(self.picture)
            self.postprocess = iWave_Postprocess()
            self.forward_trans = iWave_ForwardTrans(self.picture)
            return
        
    def initialsymbol2bin(self):
        if self.picture.picture_header.profile_id == 0:
            self.symbol2bin = BEE_Symbol2Bin()
            self.symbol2bin.update(self.picture)
            return

        if self.picture.picture_header.profile_id == 1:
            self.symbol2bin = NIC_Symbol2Bin(self.net, self.context)
            return
        
        if self.picture.picture_header.profile_id == 2:
            return

    def initialbin2bits(self):
        if self.picture.picture_header.profile_id == 0:
            self.bin2bits = BEE_Bin2Bits()
            self.bin2bits.update()
            return

        if self.picture.picture_header.profile_id == 1:
            self.bin2bits = NIC_Bin2Bits(self.picture.picture_header)
            self.postprocess = NIC_Postprocess()
            return

    def initialbits2bin(self):
        if self.picture.picture_header.profile_id == 0:
            self.bits2bin = BEE_Bits2Bin()
            self.bits2bin.update(self.picture)
            return

        if self.picture.picture_header.profile_id == 1:
            self.bits2bin = NIC_Bits2Bin(self.picture)
            return
        
        if self.picture.picture_header.profile_id == 2:
            self.bits2bin = iWave_Bits2Bin(self.picture)
            return

    def initialbin2symbol(self):
        if self.picture.picture_header.profile_id == 0:
            self.bin2symbol = BEE_Bin2Symbol()
            self.bin2symbol.update(self.picture)
            return

        if self.picture.picture_header.profile_id == 1:
            self.bin2symbol = NIC_Bin2Symbol(self.picture.picture_header)
            return 
        
        if self.picture.picture_header.profile_id == 2:
            self.bin2symbol = iWave_Bin2Symbol()
            return

    def initialinversetrans(self):
        if self.picture.picture_header.profile_id == 0:
            self.inverse_trans = BEE_InverseTrans()
            self.inverse_trans.update(self.picture)
            return

        if self.picture.picture_header.profile_id == 1:
            self.inverse_trans = NIC_InverseTrans(self.picture.picture_header)
            return
        
        if self.picture.picture_header.profile_id == 2:
            self.inverse_trans = iWave_InverseTrans(self.picture.picture_header)
            return

    def initialpostprocess(self):
        if self.picture.picture_header.profile_id == 0:
            self.postprocess = BEE_Postprocess()
            return
        
        if self.picture.picture_header.profile_id == 1:
            self.postprocess = NIC_Postprocess()
            return

        if self.picture.picture_header.profile_id == 2:
            self.postprocess = iWave_Postprocess()
            return